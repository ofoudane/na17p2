
### Les dépendances fonctionnels élémentaires de chaque relation du MLD.
Les DFE exprimées ci-dessous ne mentionneront pas les attributs retrouvés par transitivité et qui figurent en dehors de la relation étudiée.

#### Karatéka:
* référence -> nom
* référence -> prenom
* référence -> poids
* référence -> taille
* référence -> date_naissance
* référence -> ceinture
* référence -> photo

* (nom, prenom, date_naissance, poids) -> référence.
* (nom, prenom, date_naissance, poids) -> taille.
* (nom, prenom, date_naissance, poids) -> photo.
* (nom, prenom, date_naissance, poids) -> ceinture.

* (nom, prenom, date_naissance, taille) -> référence.
* (nom, prenom, date_naissance, taille) -> poids.
* (nom, prenom, date_naissance, taille) -> photo.
* (nom, prenom, date_naissance, taille) -> ceinture.

Cette relation est une 3FN, car:
    - Aucune partie de la candidate ne détermine une sous-partie des attributs en dehors de la clé.
    - Aucune partie en dehors de la clé, ne détermine à elle même un attribut non clé.
*J'ai pris la décision de considérer que deux joueurs peuvent avoir le même nom, prénom et date de naissance, car cette contrainte n'a pas été déclarée dans la NDC.*


#### Mouvement:
* nom_japonais -> nom_francais
* nom_japonais -> schema
* nom_francais -> schema
* nom_francais -> nom_japonais

Cette relation est également en BCNF, car :
    - Elle est 1FN, vu qu'elle possède une clé et que tout les attributs sont atomiques.
    - Elle est 2FN, car toutes les clés candidates sont composées d'un seul attribut, donc aucun autre attribut ne peux dépendre d'une partie de la clé vu qu'il n'y'a qu'un seul attribut.
    - Elle est 3FN, car 'schéma' est le seul attribut qui ne fait pas partie des clés candidates et il ne détermine ni nom_francais, ni nom_japonais.
    - Elle est BCNF, car 'schéma' ne determine ni nom_francais, ni nom_japonais.
*Ici, j'ai pris le choix de ne pas considérer schéma->nom_francais, car un schéma (image) peut contenir plusieurs mouvements à la fois.*

Ayant le choix entre nom_francais et nom_japonais, j'ai décidé de considérer le nom_japonais comme clé primaire conformément à l'énoncé. 

#### sous_categorie:
nom -> categorie_parent.
Il s'agit ici également d'une relation en BCNF.


#### famille_kata:
nom_japonais -> nom_francais
nom_francais -> nom_japonais

Il s'agit d'une BCNF, et je choisis nom_japonais comme clé primaire conformément à l'énoncé.

#### kata:
nom_japonais -> nom_francais
nom_japonais -> description
nom_japonais -> ceinture
nom_japonais -> nb_dans
nom_japonais -> schema
nom_japonais -> famille_kata 

nom_francais -> nom_japonais
nom_francais -> description
nom_francais -> ceinture
nom_francais -> nb_dans
nom_francais -> schema
nom_francais -> famille_kata 

Cette relation est en BCNF, car :
    - Elle est 1FN, vu qu'elle possède une clé et que tout les attributs sont atomiques.
    - Elle est 2FN, car toutes les clés candidates (nom_japonais et nom_francais) sont composées d'un seul attribut.
    - Elle est 3FN, car tous les autres attributs, à part nom_japonais et nom_francais ne déterminent aucun autre attribut en dehors de la clé.
    - Elle est BCNF, car aucun autre attribut non clé ne détermine ni nom_francais, ni nom_japonais.

*J'ai fait le choix de considérer nom_japonais comme clé primaire conformément à l'énoncé.*

#### presenter_kata: 
J'ai supposé ici qu'une vidéo peut éventuellement décrire deux kata différents et on peut donc pas écrire la DFE video_kata -> nom_kata. On ne peut pas non plus écrire nom_kata -> video_kata, car une kata peut avoir plusieurs vidéos.
Cette relation est donc en BCNF.

#### competition_kata: 
reference -> nom
reference -> debut
reference -> fin
reference -> lieu
reference -> site_web
reference -> mail_contact
reference -> club

(nom, debut) -> reference
(nom, debut) -> fin
(nom, debut) -> lieu
(nom, debut) -> site_web
(nom, debut) -> mail_contact
(nom, debut) -> club

(nom, fin) -> reference
(nom, fin) -> debut
(nom, fin) -> lieu
(nom, fin) -> site_web
(nom, fin) -> mail_contact
(nom, fin) -> club

Cette relation est également en BCNF, car il s'agit ici d'un ensemble de DFE sous format K -> A où k est soit (nom, debut), soit (nom, fin) soit référence.

#### posséder_image_kata:
Ici, j'ai supposé qu'une image peut être utilisée par plusieurs compétitions en même temps (par exemple logo de la compétition ou les sponsors). Par conséquent on pourra pas écrire image -> competition.
Cette relation est aussi en BCNF.

#### confrontation_kata:
(perdant, competition) -> score_gagnant
(perdant, competition) -> score_perdant
(perdant, competition) -> date_confrontation
(perdant, competition) -> numero_salle
(perdant, competition) -> adresse_salle
(perdant, competition) -> tour
(perdant, competition) -> kata
(perdant, competition) -> gagnant

Il s'agit ici d'une relation en BCNF, car à chaque confrontation, un karatéka est éliminé. Le karatéka éliminé ne peut plus participer à la même compétition. La référence du perdant et la compétition déterminent donc l'ensemble d'attributs d'une confrontation.

#### confrontation_kata_mixte:
(perdant, competition) -> score_gagnant
(perdant, competition) -> score_perdant
(perdant, competition) -> date_confrontation
(perdant, competition) -> numero_salle
(perdant, competition) -> adresse_salle
(perdant, competition) -> tour
(perdant, competition) -> kata
(perdant, competition) -> gagnant
(perdant, competition) -> competition

Il s'agit ici d'une relation en BCNF, car à chaque confrontation, un karatéka est éliminé. Le karatéka éliminé ne peut plus participer à la même compétition. La référence du perdant et la compétition déterminent donc l'ensemble d'attributs d'une confrontation.

#### mouvement_kata:
(nom_kata, mouvement) -> position
(nom_kata, position) -> mouvement
(nom_kata, mouvement) -> Tous les attributs determinés par kata.

Il s'agit d'une BCNF, car tous les attributs appartienent au sous-ensemble des clés candidates. *Ayant le choix entre deux clés, j'ai choisi la clé (nom_kata, position), car j'estime qu'elle est plus stable que (nom_kata, mouvement) où un changement du nom de mouvement entraînerai la modification de tous les tuples de cette relation.*



#### competition_kumite: 
reference -> nom
reference -> debut
reference -> fin
reference -> lieu
reference -> site_web
reference -> mail_contact
reference -> club

(nom, debut) -> reference
(nom, debut) -> fin
(nom, debut) -> lieu
(nom, debut) -> site_web
(nom, debut) -> mail_contact
(nom, debut) -> club

(nom, fin) -> reference
(nom, fin) -> debut
(nom, fin) -> lieu
(nom, fin) -> site_web
(nom, fin) -> mail_contact
(nom, fin) -> club

Cette relation est également en BCNF, car il s'agit ici d'un ensemble de DFE sous format K -> A où k est soit (nom, debut), soit (nom, fin) soit référence. *J'ai décidé d'utiliser la clé référence, car elle n'est composée que d'un seul attribut*.


#### posséder_image_kumite:
Sous l'hypothèse qu'une même image peut être partagée (logo ou sponsors) entre plusieurs compétitions, on pourra pas déduire que image -> compétition. Cette relation est donc en BCNF.

#### confrontation_kumite et confrontation_kumite_mixte:
(participant1, participant2, competition) -> date_confrontation
(participant1, participant2, competition) -> numero_salle
(participant1, participant2, competition) -> adresse_salle
(participant1, participant2, competition) -> tour

Il s'agit ici d'une relation en BCNF, car un match entre deux karatéka ne peut pas avoir lieu dans la même compétition, vu qu'à l'issue d'une confrontation un karatéka est éliminé.
*Je rappelle ici que pour les confrontations de kumite, le gagnant et le perdant d'une confrontation sont décidés en fonction du score marqué par chaqu'un des participants.*

#### coup_kumite et coup_kumite_mixte:
id -> confrontation
id -> participant1
id -> participant2
id -> competition
id -> source

C'est une relation en BCNF, car il s'agit d'une relation à une seule clé candidate et qui est sous la forme K -> A où K est une clé.

#### autoriser_coup_kumite:
(competition, sous_categorie) -> points
C'est une relation en BCNF.

#### interdire_coup_kumite:
Aucune DFE dans cette relation.
C'est une relation en BCNF.

#### competition_tameshi_wari: 
reference -> nom
reference -> debut
reference -> fin
reference -> lieu
reference -> site_web
reference -> mail_contact
reference -> club

(nom, debut) -> reference
(nom, debut) -> fin
(nom, debut) -> lieu
(nom, debut) -> site_web
(nom, debut) -> mail_contact
(nom, debut) -> club

(nom, fin) -> reference
(nom, fin) -> debut
(nom, fin) -> lieu
(nom, fin) -> site_web
(nom, fin) -> mail_contact
(nom, fin) -> club

Cette relation est également en BCNF, car il s'agit ici d'un ensemble de DFE sous format K -> A où k est soit (nom, debut), soit (nom, fin) soit référence. *J'ai décidé d'utiliser la clé référence, car elle n'est composée que d'un seul attribut*.


#### posséder_image_kumite:
Sous l'hypothèse qu'une même image peut être partagée (logo ou sponsors) entre plusieurs compétitions, on pourra pas déduire que image -> compétition. Cette relation est donc en BCNF.

#### confrontation_tameshi_wari et confrontation_tameshi_wari_mixte:
(perdant, competition) -> score_gagnant
(perdant, competition) -> score_perdant
(perdant, competition) -> date_confrontation
(perdant, competition) -> numero_salle
(perdant, competition) -> adresse_salle
(perdant, competition) -> tour
(perdant, competition) -> gagnant

Il s'agit ici d'une relation en BCNF, car à chaque confrontation, un karatéka est éliminé. Suite à une défaite, le karatéka éliminé ne peut plus participer à cette compétition. La référence du perdant et la compétition déterminent donc l'ensemble d'attributs d'une confrontation. De plus, aucun attribut.

#### action_tameshi_wari:
C'est une BCNF, car elle n'est composée que d'un seul attribut.

#### evaluer_action_tameshi_wari:
Aucune DFE dans cette relation, c'est donc une BCNF.



#### competition_mixte:
reference -> nom
reference -> debut
reference -> fin
reference -> lieu
reference -> site_web
reference -> mail_contact
reference -> club

(nom, debut) -> reference
(nom, debut) -> fin
(nom, debut) -> lieu
(nom, debut) -> site_web
(nom, debut) -> mail_contact
(nom, debut) -> club

(nom, fin) -> reference
(nom, fin) -> debut
(nom, fin) -> lieu
(nom, fin) -> site_web
(nom, fin) -> mail_contact
(nom, fin) -> club

Cette relation est également en BCNF, car il s'agit ici d'un ensemble de DFE sous format K -> A où k est soit (nom, debut), soit (nom, fin) soit référence. *J'ai décidé d'utiliser la clé référence, car elle n'est composée que d'un seul attribut*.

#### posséder_image_competition_mixte:
Sous l'hypothèse qu'une même image peut être partagée (logo ou sponsors) entre plusieurs compétitions, on pourra pas déduire la DFE image -> compétition. Cette relation est donc en BCNF.

#### autoriser_coup_kumite_mixte:
(competition, sous_categorie) -> points
C'est une relation en BCNF.

#### interdire_coup_kumite_mixte:
Aucune DFE dans cette relation.
C'est une relation en BCNF.


#### club:
nom -> site_web
nom -> adresse_dirigeant
C'est une relation en BCNF, car il s'agit ici de DFE sous forme K -> A où K est toujours une clé.
