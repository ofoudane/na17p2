# Note de clarification (Karaté Kid)

## Objets
### Karateka
- Une référence : identifiant.
- Un nom.
- Un prénom.
- Le poids en kilogramme.
- La taille en cm.
- La date de naissance.
- Zéro ou une photo.
- La couleur de sa ceinture : du jaune au rouge.
- Un nombre de dans (grade).
- L'historique de confrontations.
- Les katas qu'il maîtrise.

### Kata
- Le nom en japonais : identifiant.
- La traduction du nom en français.
- Une description en français.
- Une ou plusieurs vidéos de démonstration.
- Un et un seul schéma avec tous les mouvements : une image.
- Un ou plusieurs mouvements.
- La position de chaque mouvement dans ce kata.
- Le nombre de mouvements.
- La couleur de la ceinture : du jaune au rouge.
- Le nombre de "dans" correspondant à l'apprentissage de ce kata : Un entier positif.
- Une et une seule famille de kata.


### Mouvement du karaté
- Un nom en japonais : identifiant.
- La traduction du nom en français.
- Un et un seul schéma : une image.
- Une ou plusieurs sous-catégories.

### Sous-catégorie mouvement
- Le nom de la sous-catégorie : identifiant.
- Une et une seule catégorie parent.

### Famille de kata
- Un nom en japonais : identifiant.
- La traduction du nom en français.


### Confrontation
- Le karatéka gagnant.
- Le karatéka perdant.
- Le score du karatéka gagnant.
- Le score du karatéka perdant.
- La date de la confrontation : le jour et l'heure de la confrontation.
- Une et une salle de confrontation : Numéro et adresse de la salle.
- Un et un seul tour : qualifications, quart de finale, demi finale, finale ou confrontation pour la 3ème place.
- Le type de la confrontation: kata, kumite ou tameshi wari.
    ##### Confrontation de kata
    - Le nom du kata.
    ##### Confrontation de kumite
    - Zéro, un ou plusieurs coups ayant marqué au moins un point.
    ##### Confrontation tameshi wari
    - Une ou plusieurs actions (cassage de planches de bois, dalles de ciment, de glace...).



### Coup de kumite
- Un seul karatéka source.
- Une seule sous-catégorie de mouvements.
- La confrontation de kumite dans laquelle ce coup a été réalisé. 


### Compétition
- Un nom.
- La date de début.
- La date de fin.
- Le lieu de la compétition : Chaîne de caractères libre.
- Au moins deux karatékas.
- Une ou plusieurs confrontations.
- Le classement final des karatékas.
- Zéro, une ou plusieurs photos.
- Au plus un site web sous format URL.
- Au plus un mail de contact sous format mail.
- Le type de compétition : kumite, kata, tameshi wari ou mixte.
    ##### Compétition kumite
    - Zéro, une ou plusieurs sous-catégories de mouvements interdites.
    - Les points emportés après chaque mouvement d'une sous-catégorie de mouvements autorisée.
    ##### Compétition mixte
    - Zéro, une ou plusieurs sous-catégories de mouvements interdites.
    - Une ou plusieurs sous-catégories de mouvements autorisés.
    - Les points remportés après chaque mouvement d'une sous-catégorie de mouvements autorisés.


### Club de karaté
- Un nom : identifiant.
- Au plus un site web : URL.
- L'adresse du dirigeant: Chaîne de caractères libre.
- La liste de professeurs de karatékas inscrits.
- Les compétitions qu'ils organisent.
- Les compétitions organisées.

## Clarifications:
- L'age du karatéka sera calculé à partir de sa date de naissance.
- Le nombre de "dans" d'un karatéka est calculé à partir des kata qu'il maîtrise. Ce nombre reste à zéro jusqu'à ce que le karatéka aie obtenu une ceinture noir.
- Une image, schéma ou vidéo est représenté par une chaîne de caractère qui peut être soit un lien vers un site externe ou un identifiant d'image interprété par un système tiers.
- Le nombre de mouvements d'un kata sera calculé à partir de la liste de mouvements de ce kata.
- Dans une confrontation de kumite, le score du gagnant et du perdant est calculé à partir des coups réalisés dans cette confrontation.
- La salle d'une confrontation, le lieu d'une compétition ou l'adresse d'un dirigeant d'un club seront des chaînes de caractères libres sans aucun format prédéfini.
- On peut distinguer les compétition réalisées, futures ou en cours grâce à la date de début et de fin des compétitions.
- Les participants d'une compétition seront retrouvés à partir de confrontation enregistrées pour ce tournoi.
- Pour les catégories de mouvement, le choix d'une sous-catégorie impliquera automatiquement le choix de la catégorie parent.
- Les karatékas seront classés par tour d'élimination puis en fonction de leur score total.
- La cible d'un coup de kumite est déduie de la confrontation dans lequel ce coup a été donné.
- Un professeur de karatéka peut appartenir à plusieurs clubs en même temps.

## Contraintes:
- La couleur d'une ceinture va du jaune au noir.
- Le poids et la taille d'un karatéka est forcément positive.
- La taille d'un karatéka ne peut pas dépasser 3m et doit être supérieure à 30cm.
- Le poids d'un karatéka doit être supérieur à 10kg et doit être inférieur à 1000kg. 
- Le nombre de "dans" d'un karatéka est toujours positif ou nul.
- Le nombre de "dans" d'une kata doit être strictement positif.
- Un kata appartient à une seule famille de kata.
- Une confrontation oppose nécessairement deux karatékas distincts.
- Un joueur éliminé à l'une des confrontations ne pourra plus participer à une autre confrontation dans la même compétition.
- La source d'un coup de kumite doit être l'un des participants à la confrontation dans laquelle il a été réalisé.
- Dans une compétition de kumite, seules les sous-catégories de mouvements non interdits peuvent avoir un nombre de points à attribuer au karatéka ayant effectué ce mouvement.
- Dans une compétition de kumite, toute les confrontations sont des confrontations de kumite. Le même principe s'applique aux compétitions de tameshi wari ou de kata. En revanche, les compétitions mixte peuvent avoir des confrontations de types différents. 
- Une compétition peut aux maximum avoir 2 confrontation en demi finale, 4 confrontation en quart de finale, 1 confrontation en finale, 1 confrontation pour la troisième place et un nombre quelconque de confrontation de qualification.
- Le tour d'une confrontation ne peux être autre que "Qualification", "Quart de finale", "Demi finale", "Match pour la troisième place" ou "finale".
- Deux mouvements dans un même kata ne peuvent pas être à la même position.
- Dans une confrontation, le score du gagnant est nécessairement supérieur au score du perdant.
- Deux familles de kata ne peuvent pas avoir le même nom en français.

## Hypothèses:
- J'estime qu'il est pertinent d'enregister l'heure et la salle de chaque confrontation.
- Chaque karatéka a un numéro qui permet de l'identifier.
- Deux karatéka peuvent avoir le même nom et le même prénom.
- Deux familles de kata ne peuvent pas avoir le même nom en français.
- Les images, schémas ou vidéos ne sont pas limités à des liens URL, mais peuvent aussi être des identifiants interprété par une application tiers par exemple.
- Le nombre de "dans" d'un karatéka peut être calculé à partir des kata qu'il maîtrise. 
- Chaque compétition est identifiée par une référence unique.
- Deux compétitions ne peuvent pas avoir le même nom et une même date de début, ou le même nom et la même date de fin.
- Un mouvement peut être répété plusieurs fois dans un kata.
- Comme pour une compétition de kumite, une compétition mixte peut également avoir une liste de sous-catégories de mouvements interdits ainsi que le nombre de points pour chaque mouvement autorisé dans une confrontation de kumite dans cette compétition.
- On ne s'interesse qu'aux résultats de confrontations, c'est-à-dire qu'on n'enregistre pas les confrontations non terminées.
- Les karatékas seront classés par tour d'élimination puis en fonction de leur score total.
- Deux katas distincts ne peuvent pas avoir le même nom ni en japonais, ni en français.
- Deux familles de kata distinctes ne peuvent pas avoir le même nom ni en japonais, ni en français.
- Le type d'une compétition est forcément "kumite", "kata", "tameshi wari" ou "mixte".

## Rôles:
#### Administrateur:
- Ajouter, modifier ou supprimer des clubs.
- Ajouter, modifier ou supprimer des karatéka.
- Ajouter, modifier ou supprimer des compétitions.
- Ajouter, modifier ou supprimer des kata.
- Ajouter, modifier ou supprimer des familles de kata.
- Ajouter, modifier ou supprimer un mouvement de karaté.
- Supprimer une confrontation.
- Ajouter, modifier ou supprimer une sous-catégorie de mouvement.
- Supprimer un coup de kumite enregistré pendant une confrontation.

#### Chaque club pourra:
- Modifier une compétition qu'il a organisé.
- Modifier ou supprimer son club.
- Modifier les compétitions **en cours**, **organisées** par ce club.
- Supprimer une compétition organisée par ce club.
- Définir les sous-catégories de mouvements interdits dans une compétition de kumite ou une compétition mixte.
- Définir les sous-catégories de mouvements autorisés dans une compétition de kumite, ainsi que le nombre de points rapporté par chaque coup de kumite.
- Ajouter et supprimer les différents coups de kumite portés dans une confrontation de kumite que ce club a organisé.
- Ajouter, supprimer ou modifier les confrontations qui se sont déroulées dans une compétition organisée par ce club.