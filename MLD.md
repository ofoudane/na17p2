#### MLD (Karaté-Kid)

**karateka** (#reference: String, nom: String, prenom: String, poids: int[10, 1000], taille: int[30, 300], date_naissance: Date, ceinture: String [jaune...noir], photo: String)  avec nom, prenom, poids, taille, date_naissance, ceinture not null.

// Package mouvement: 
**mouvement** (#nom_japonais: String, nom_francais: String, schema: String) avec nom_francais unique not null et schema not null.

**sous_categorie** (#nom: String, categorie_parent: String) avec categorie_parent not null

**sous_categorie_mouvement** (#mouvement => mouvement, #sous_cat => sous_categorie)


// Package kata: 
**famille_kata** (#nom_japonais: String, nom_francais: String) avec nom_francais unique not null

**kata** (#nom_japonais: String, nom_francais: String, description: String, ceinture: String [jaune...noir], nb_dans: int > 0, schema: String, famille_kata => famille_kata) avec nom_francais unique et nom_francais, description, ceinture, nb_dans, schema et famille_kata not null.

**presenter_kata** (#nom_kata => kata, #video_kata: String)

**competition_kata** (#reference: String, nom: String, debut: Date, fin: Date, lieu: String, site_web: String, mail_contact: String, club => club) avec nom, debut, fin, lieu et club not null et reference immutable et debut <= fin. Avec (nom, debut) unique et (nom, fin) unique.

**posséder_image_kata** (#competition => competition_kata, #image: String)

**confrontation_kata** (#perdant => karateka, #competition => competition_kata, gagnant => karateka, score_gagnant : int > 0, score_perdant: int >= 0, date_confrontation: Timestamp, numero_salle : String, adresse_salle : String, tour : {Qualif, Quart, Demi, 3emeP, Finale}, kata => kata) avec gagnant, score_gagnant, score_perdant, date_confrontation, numero_salle, adresse_salle, tour et kata not null ET (gagnant <> perdant) ET (score_gagnant > score_perdant). 

**confrontation_kata_mixte** (#perdant => karateka, #competition => competition_mixte, gagnant => karateka, score_gagnant : int > 0, score_perdant: int >= 0, date_confrontation: Timestamp, numero_salle : String, adresse_salle : String, tour : {Qualif, Quart, Demi, 3emeP, Finale}, kata => kata) avec gagnant, score_gagnant, score_perdant, date_confrontation, numero_salle, adresse_salle, tour et kata not null ET (gagnant <> perdant) ET (score_gagnant > score_perdant).

**maitrise_kata** (#nom_kata => kata, #karateka => karateka)

**mouvement_kata** (#nom_kata => kata, #position: int > 0, mouvement => mouvement) avec le couple (nom_kata, mouvement) clé et mouvement not null.



// Package Kumite : 
**competition_kumite** (#reference: String, nom: String, debut: Date, fin: Date, lieu: String, site_web: String, mail_contact: String, club => club) avec nom, debut, fin, lieu et club not null et reference immutable et debut <= fin. Avec (nom, debut) unique et (nom, fin) unique.

**posséder_image_kumite** (#competition => competition_kumite, #image: String)

**confrontation_kumite** (#participant1 => karateka, #participant2 => karateka, #competition => competition_kumite, date_confrontation: Timestamp, numero_salle : String, adresse_salle : String, tour : {Qualif, Quart, Demi, 3emeP, Finale}) avec date_confrontation, numero_salle, adresse_salle, tour not null ET (participant1 <> participant2). 

**confrontation_kumite_mixte** (#participant1 => karateka, #participant2 => karateka, #competition => competition_mixte, date_confrontation: Timestamp, numero_salle : String, adresse_salle : String, tour : {Qualif, Quart, Demi, 3emeP, Finale}) avec date_confrontation, numero_salle, adresse_salle, tour not null ET (participant1 <> participant2). 


**coup_kumite** (#id: int, participant1 => confrontation_kumite.participant1, participant2 => confrontation_kumite.participant2, competition => confrontation_kumite.competition, source => karateka, sous_cat => sous_categorie) avec participant1, participant2, competition, source et sous_cat not null ET (source = participant1 OU source = participant2)

**coup_kumite_mixte** (#id: int, participant1 => confrontation_kumite_mixte.participant1, participant2 => confrontation_kumite_mixte.participant2, competition => confrontation_kumite_mixte.competition, source => karateka, sous_cat => sous_categorie) avec participant1, participant2, competition, source et sous_cat not null ET (source = participant1 OU source = participant2)


**autoriser_coup_kumite** (#competition => competition_kumite, #sous_categorie => sous_categorie, points: int > 0) avec points not null.

**interdire_coup_kumite** (#competition => competition_kumite, #sous_categorie => sous_categorie)



// Package Tameshi_wari
**competition_tameshi_wari** (#reference: String, nom: String, debut: Date, fin: Date, lieu: String, site_web: String, mail_contact: String, club => club) avec nom, debut, fin, lieu et club not null et reference immutable et debut <= fin. Avec (nom, debut) unique et (nom, fin) unique.

**posséder_image_tameshi_wari** (#competition => competition_tameshi_wari, #image: String)

**confrontation_tameshi_wari** (#perdant => karateka, #competition => competition_tameshi_wari, gagnant => karateka, score_gagnant : int > 0, score_perdant: int >= 0, date_confrontation: Timestamp, numero_salle : String, adresse_salle : String, tour : {Qualif, Quart, Demi, 3emeP, Finale}) avec gagnant, score_gagnant, score_perdant, date_confrontation, numero_salle, adresse_salle et tour not null ET (gagnant <> perdant) ET (score_gagnant > score_perdant).

**confrontation_tameshi_wari_mixte** (#perdant => karateka, #competition => competition_mixte, gagnant => karateka, score_gagnant : int > 0, score_perdant: int >= 0, date_confrontation: Timestamp, numero_salle : String, adresse_salle : String, tour : {Qualif, Quart, Demi, 3emeP, Finale}) avec gagnant, score_gagnant, score_perdant, date_confrontation, numero_salle, adresse_salle et tour not null ET (gagnant <> perdant) ET (score_gagnant > score_perdant).


**action_tameshi_wari** (#action: String)

**evaluer_action_tameshi_wari** (#perdant => confrontation_tameshi_wari.perdant, #competition => confrontation_tameshi_wari.competition, #action => action_tameshi_wari)

**evaluer_action_tameshi_wari_mixte** (#perdant => confrontation_tameshi_wari_mixte.perdant, #competition => confrontation_tameshi_wari_mixte.competition, #action => action_tameshi_wari)



// Club et compétition mixte:
**competition_mixte** (#reference: String, nom: String, debut: Date, fin: Date, lieu: String, site_web: String, mail_contact: String, club => club) avec nom, debut, fin, lieu et club not null et reference immutable et debut <= fin. Avec (nom, debut) unique et (nom, fin) unique.

**posséder_image_competition_mixte** (#competition => competition_mixte, #image: String)

**autoriser_coup_kumite_mixte** (#competition => competition_mixte, #sous_categorie => sous_categorie, points: int > 0) avec points not null.

**interdire_coup_kumite_mixte** (#competition => competition_mixte, #sous_categorie => sous_categorie)


**club** (#nom: String, site_web: String, adresse_dirigeant: String) avec adresse_dirigeant not null.

**professeur_club** (#karateka => karateka, #club => club)

## Contraintes:
#### Un mouvement appartient à au moins une sous-catégorie
*R1* = Projection(mouvement, nom_japonais)
*R2* = Jointure(R1, sous_categorie_mouvement, R1.nom_japonais = sous_categorie_mouvement.mouvement)
*R3* = Projection(R2, mouvement)
**Contrainte** : R1 <=> R3

#### Un kata a au moins une vidéo de présentation:
*R1* = Projection(kata, #nom_japonais)
*R2* = Jointure(R1, presenter_kata, presenter_kata.nom_kata = R1.nom_japonais)
*R3* = Projection(R2, nom_kata)
**Contrainte** : R1 <=> R3

#### Un kata a au moins un mouvement:
*R1* = Projection(kata, nom_japonais)
*R2* = Jointure(R1, mouvement_kata, mouvement_kata.nom_kata = R1.nom_japonais)
*R3* = Projection(R2, nom_kata)
**Contrainte** : R1 <=> R3

#### Au moins une sous-catégorie de coups est autorisée et récompensée dans une compétition de kumite:
*R1* = Projection(competition_kumite, reference)
*R2* = Jointure(R1, autoriser_coup_kumite, autoriser_coup_kumite.competition = R1.reference)
*R3* = Projection(R2, competition)
**Contrainte** : R1 <=> R3


#### Au moins une action de tameshi wari est évaluée dans une confrontation de tameshi wari standard:
*R1* = Projection(confrontation_tameshi_wari, perdant, competition)
*R2* = Jointure(R1, evaluer_action_tameshi_wari, evaluer_action_tameshi_wari.perdant = R1.perdant AND evaluer_action_tameshi_wari.competition = R1.competition)
*R3* = Projection(R2, perdant, competition)
**Contrainte** : R1 <=> R3

#### Au moins une action de tameshi wari est évaluée dans une confrontation de tameshi wari mixte:
*R1* = Projection(confrontation_tameshi_wari_mixte, perdant, competition)
*R2* = Jointure(R1, evaluer_action_tameshi_wari_mixte, evaluer_action_tameshi_wari_mixte.perdant = R1.perdant AND evaluer_action_tameshi_wari_mixte.competition = R1.competition)
*R3* = Projection(R2, perdant, competition)
**Contrainte** : R1 <=> R3


#### Une sous catégorie de mouvements interdite dans une compétition de kumite ne peut pas donner lieu à un nombre de points (autorisée) :
*R1* = Jointure(autoriser_coup_kumite, interdire_coup_kumite, autoriser_coup_kumite.competition = interdire_coup_kumite.competition and autoriser_coup_kumite.sous_categorie = interdire_coup_kumite.sous_categorie)
**Contrainte**: R1 <=> {}

#### Une sous catégorie de mouvements interdits dans une compétition mixte ne peut pas donner lieu à un nombre de points (autorisée) :
*R1* = Jointure(autoriser_coup_kumite_mixte, interdire_coup_kumite_mixte, autoriser_coup_kumite_mixte.competition = interdire_coup_kumite_mixte.competition and autoriser_coup_kumite_mixte.sous_categorie = interdire_coup_kumite_mixte.sous_categorie)
**Contrainte**: R1 <=> {}


#### Un joueur éliminé à l'une des confrontations ne pourra plus participer à une confrontation dans la même compétition :
##### Un joueur éliminé d'une compétition de kata ne pourra plus participer à aucune confrontation dans cette compétition :
*R1* = Projection(confrontation_kata, perdant, competition, date_confrontation)
GAGNER_APRES_AVOIR_PERDU = Jointure(R1, confrontation_kata, R1.date_confrontation < confrontation_kata.date_confrontation AND R1.competition = confrontation_kata.competition AND R1.perdant = confrontation_kata.gagnant)
**Contrainte**: GAGNER_APRES_AVOIR_PERDU <=> {}

##### Un joueur éliminé d'une compétition de Tameshi wari ne pourra plus participer à aucune confrontation dans cette compétition :
*R1* = Projection(confrontation_tameshi_wari, perdant, competition_kumite, date_confrontation)
*R2* = Projection(confrontation_tameshi_wari, perdant, gagnant, competition_kumite, date_confrontation)
DEUX_PARTICIPATIONS_POUR_UN_PERDANT = Jointure(R1, R2, R1.date_confrontation < R2.date_confrontation AND R1.competition = R2.competition AND R1.perdant = R2.gagnant)
**Contrainte**: DEUX_PARTICIPATIONS_POUR_UN_PERDANT <=> {}

##### Un joueur éliminé d'une compétition de kumite ne pourra plus participer à aucune confrontation dans cette compétition :
Cette contrainte ne peux pas être exprimée en MLD, car le gagnant ou le perdant sont définis à partir des coups de kumite qu'ils ont marqué.

##### Un joueur éliminé d'une compétition mixte ne pourra plus participer à aucune prochaine confrontation dans cette compétition :
Le même problème se pose ici. Une compétition mixte peut potentiellement contenir une confrontation de kumite. Or, on ne peut exprimé la contrainte d'élimination de joueur après sa perte en compétition de kumite en MLD. Par conséquent, on ne pourra pas non plus exprimer cette contrainte sur les compétitions mixtes en MLD.


#### Une compétition peut avoir au maximum 2 confrontations en demi-finale, 4 en quart de finale, 1 en finale et 1 pour la 3ème place:
##### Pour les compétitions non mixte (ex: Tameshi wari)
###### Une compétition non mixte ne peut pas avoir plus d'une confrontation en finale ou pour la 3ème place :
*R0* = Projection(confrontation_tameshi_wari, perdant, competition, tour)
*R1* = Renommage(R0, perdant_1, competition_1, tour_1)
*R2* = Renommage(R0, perdant_2, competition_2, tour_2)
*DEUX_CONF_FINALE_OU_3EME_PLACE* = Jointure(R1, R2, perdant_1 != perdant_2 and competition_1 = competition_2 and (tour_1 = Finale and tour_2 = Finale) or (tour_1 = 3emeP and tour_2 = 3emeP))
**Contrainte**: DEUX_CONF_FINALE_OU_3EME_PLACE <=> {}

###### Une compétition non mixte ne peut pas avoir plus de 2 confrontations en demi finale :
*R0* = Projection(confrontation_tameshi_wari, perdant, competition_tameshi_wari, tour)
*R1* = Renommage(R0, perdant_1, competition_1, tour_1)
*R2* = Renommage(R0, perdant_2, competition_2, tour_2)
*R3* = Renommage(R0, perdant_3, competition_3, tour_3)

*DEUX_CONF_DEMI_FINALE* = Jointure(R1, R2, perdant_1 != perdant_2 and competition_1 = competition_2 and tour_1 = Demi and tour_2 = Demi)
*TROIS_CONF_DEMI_FINALE* = Jointure(DEUX_CONF_DEMI_FINALE, R3, perdant_1 != perdant_3 and perdant_2 != perdant_3 and competition_1 = competition_3 and tour_3 = Demi)
**Contrainte**: TROIS_CONF_DEMI_FINALE <=> {}

###### Une compétition non mixte ne peut pas avoir plus de 4 confrontations en quart de finale :
*R0* = Projection(confrontation_tameshi_wari, perdant, competition_tameshi_wari, tour)
*R1* = Renommage(R0, perdant_1, competition_1, tour_1)
*R2* = Renommage(R0, perdant_2, competition_2, tour_2)
*R3* = Renommage(R0, perdant_3, competition_3, tour_3)
*R4* = Renommage(R0, perdant_4, competition_4, tour_4)
*R5* = Renommage(R0, perdant_5, competition_5, tour_5)

*DEUX_CONF* = Jointure(R1, R2, perdant_1 != perdant_2 and competition_1 = competition_2 and tour_1 = Quart and tour_2 = Quart)
*TROIS_CONF* = Jointure(DEUX_CONF, R3, perdant_1 != perdant_2 and perdant_2 != perdant_3 and competition_1 = competition_3 and tour_3 = Quart)
*QUATRE_CONF* = Jointure(TROIS_CONF, R4, perdant_1 != perdant_4 and perdant_2 != perdant_4 and perdant_3 != perdant_4 and competition_1 = competition_4 and tour_4 = Quart)
*CINQ_CONF* = Jointure(QUATRE_CONF, R5, perdant_1 != perdant_5 and perdant_2 != perdant_5 and perdant_3 != perdant_5 and perdant_4 != perdant_5 and competition_1 = competition_5 and tour_5 = Quart)

**Contrainte**: CINQ_CONF <=> {}

Le même principe peut être utilisé pour les compétitions de kata ou de kumite.
Pour les compétitions mixte, on peut suivre le même principe, mais en regardant dans les différentes tables de confrontations mixtes.