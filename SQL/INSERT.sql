BEGIN TRANSACTION;
INSERT INTO public.karateka (reference, nom, prenom, poids, taille, date_naissance, ceinture, photo) VALUES 
    ('FRA2115', 'OMARI', 'Mehdi', 65, 175, to_date('23-02-1986', 'DD-MM-YYYY'), 'noire', NULL),
    ('CGO136',  'LESSELE', 'Opouya', 75, 185, to_date('15-09-1980', 'DD-MM-YYYY'), 'marron 1', NULL),
    ('FRA2284', 'OUISSA', 'Youssef', 73, 170, to_date('21-07-1996', 'DD-MM-YYYY'), 'verte', 'https://www.sportdata.org/wkf/competitor_pics//12917.jpg'),
    ('FRA2059', 'YOHAN', 'Paul', 80, 183, to_date('22-10-1985', 'DD-MM-YYYY'), 'jaune', NULL);

INSERT INTO mouvements.mouvement (nom_japonais, nom_francais, schema_mouvement) VALUES 
    ('Ayumi Ashi', 'avancer d''un pas', 'https://www.google.com/url?sa=i&url=https%3A%2F%2Fkarate-blog.net%2Fayumi-ashi-avancer-dun-pas%2F&psig=AOvVaw34HeRtkUkvfTTLdoRWMnin&ust=1591040932795000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCNCC0r7v3ukCFQAAAAAdAAAAABAH'),
    ('Age Zuki', 'coup de poing en remontant', 'https://www.sik-cagnes.fr/wp-content/uploads/2015/09/tsu-agetsuki.gif'),
    ('Jodan Mae Age Empi Uchi', 'coup de coude de face en remontant', 'http://skdmery.e-monsite.com/medias/images/jodan-mae-age-empi-uchi.jpg'),
    ('Oi Zuki', 'coup de poing en poursuite', 'https://i.pinimg.com/originals/b7/5c/76/b75c761bfd9198882163c730c31da25a.jpg'),
    ('Furi Uchi', 'coup frappé diagonalement de bas en haut','data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSExIWFhUXFxYXFhgYGBcdFRYYFRobFxYWGB4bHSggHR4lGxceITIhJykrLi4uGB8zODMtNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAPsAyAMBIgACEQEDEQH/xAAcAAEBAAMBAQEBAAAAAAAAAAAABgEFBwQDAgj/xABFEAABAwIEBAQDBAcFBgcAAAABAAIDBBEFEiExBgcTQRQiUWEycZEjUoGhFSQzQmKCwSVydLGyCBY1Q1OSVWOTpNHS8P/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwDuKIiAiIgIiICIiAvjU1TI2l8j2saNS5xAaPmToF4eI8dhoad9TOSI2WvYXJLiGgAdzcr+dKzEMS4jq+k3SMEkMBcIIWi9nP31NrXO5vayD+h6Diqink6UNXBI+2bKyRjjbbsfyW3BUZhnLHDo6dkTqdj5GtAM1ssxcP8AmBwN2uvrcLXYVjlVhtWyhxCQSU0nlpKpwOdzr+WKd22a3e2vqb6B0VFhpWUBERAREQEREBFquJ8cZQ00lVICWxgHKN3EkNa0X7kkBa3h3jyhrXCOKcNlJI6MnkmDhu3K7cj2vsgp0REBERAREQFglZX5cgzdZUfyrrHy0AfI9z5OtUh5cbuBEz/Kb7WFtOwsrBAREQcS/wBpSqcG0cVzlcZnEepZ0w3/AFFbz/Z+wNsNAaq5z1Lje+wbA97G2/EuWp/2jacyChaxpc90kjGgblzgwBo9ybLpPBGDCioYKawDmMGexJHUd5pDc+riUGxdi0IqBS9Qdcx9UR63yA5c21t18+IMHirKeSnmYHMeLG/Y9nD0IOoKleJmeHxegrC5uSXNQvB/ddIDJER7uc3LYq5fsgk+VWJPnw2IyuLnxukhLju7pPLGk++UBVyheTX/AA4/4ip07D7Q6BXSAiIgIiICIhQQ/NM3GHRjXPiVLmb2cxudzgexGgNvZa3inDojxBhZY0B5FTJLlaL2jZeN7iBexddup7LbcZNE1fhcAPmbPJUHf4IYyN/7zwEp7yY7Lc6Q0UYAHfqyEnN8sn5oLRERAREQEREBERBDcAM6VZitOdD4vrhv8NQwOzAe5B19lcqHkqG0mOee+Wvp2NY7sJqYm0f8zHX+bfdW4KDKIiCB5iU4fXYQHgGPxTr3++GZmfm2/wCCvCp/jvD6eejkZUyiFjS14muAYXtILHgnuDp+Kl+HOYrmMjZWQySXzBlVTMMsE4jNi+zLuaR3FjsUG/HL6h8QKoxPMol6wLpZXNEgOYODS7LodtNF6OOuIvBUpkYA+d5bHBGd5JHEAAC4JABzH2Cm8f5pOhiMsOG1L2BwYXTDotzu+FoDgXOJ9gtE01lJUxYviUbZoJI47hoeDhzpNRaM3va+Uu38x13BDovAvD/gKKKmLi9zczpHfee8lzyPa5sPkt+vjSTtkY17HBzXAFrhqCDsQvsgIiICIiAhRYcdEENRMM2PzzN1jp6JlM432lkk69h/Lv8AgscAkTVmJ1d7k1Ip2kG7enTsAbb5lxJ9wvzwBXsFHW4g8gMlqqucu3IjjORoNvRsdgP/AJXr5S0To8Lp3P0dKHzu216z3PB09WkH1QWSIiAiIgIiICIiCZ464ddWQtdGS2op3iemd26serWuGxa42BuvtwTxAK2mbIdJW/Zzt2yTM0kbudL7eyoFz/HP7KrxWhoFHVZI6ogWEMoJEc5tpZxcGuJ9PdBflSvFvFnhnx00DOtWTW6UVyAG3IMshtZrBY/OxVDWVjY4nykgtYxzzr2a3N/kpPlthmaEYhN56mrHVc9w80cb9WQMJ1DGi2ndB+6DgrqSR1WIymqqGfCCMtPFf91kY0da58zrk/S1ZSUkcTBHGxrGDZrQA0fIBfbsoqm47ETizEKaSiOZwZI/WneATltINGuLRsbIPnxC/wAVjFHR7spmOrX+me/SiDr+hcT66q1nga9pa5oc07ggEEe4KgOVM4q3V2IkDNPVOYw65hDE1ojbqT6300vddEQc7wIfonEGYaHfqlU2SSmDic0UjNZIgdi1xJcPmuiXUlzMwA1dG/pkNnhIngfb4XxeYgemYC30W24VxhtbSQ1TRl6rA4t+6dnD8CCg26IiAiIgLX8QVHTpah+nlhldr/Cwn+i2Cl+Y2G1NTQzQUr2Me8BrjIS1vTP7QXANrj8roJNjwzhmnh2fUxRQRgWBdJUusLXIubEuPsCey6hTRZGNYP3QG/QWXF8LrKzEXUNM2iayjoqqnPiGSGRh8K0tGUutmaQR5gCu1tQZREQEREBERAREQF5cSoY543QzMD43izmuFwQvUiDjPElXNS0cmCSud1JHRw0U5P7eF8sYdmsbhzAcpBtcXtouwUkDY2NY0ANa0NAGwDRYAe1lEcz6PxL6ClY5rJpahz4pSLmLw8bpXOb88obbbVbHgfiZ9R1KaqaGVtPZs7R8Dwfhlj9Wn8iD7IKxeetpGSsMcjA9jhYtcLgj3XoCIOeUuC1ODutRxuqaFxzPgzXnhcd3Ql2jm9y063uqnhriemrmZ4JQSLhzDpLGRa4e06ixNvRbpS/EPBkNQ4TxuNNUt+GohAD/AJPGz2+xQUr2hwIOoIIPuDuorlK0x009KSbUtXUQMzfF0w7My/zDr/IhfBuN4rReWrovGRj/AJ9IR1CO14TYk9tD2K1XAnFlOMQxASTCITyxyQtmBic45Mhs14Gug76+iDqiL8teDsv0gIi8WLYpFTRPmmdkjYCXE+3p6n2QebijiCGgp31M7iGNsLAXLnONmtA9Sub1PElZik8GHTwPw5k7HPeS4mSaMAHpROs0AubmuCDp29drhdN4lxxfE7wxROc+lgkNmQstl6sgO73b2+S+mFUsuLVcddPG6Kkp3Z6NjgWyyvuD1363DfKLNO90FxhGHR08McEYsyNjWNHezRbX3917QiICIiAiIgIiICIiAiIgieNmBuIYRNoHNqZogXHyZZ4SHDTXOcoy9r7r0ca8NvkdHXUYDa6C2QkkNmjFy6CSx1BvcehXj4nGbGsKYfha2skA7F2QNBPyvp6K6QaDhHiZldESGmOaI9OeJ3xRyAeYd7tvex7rfqT4m4Ze+XxlHIIqxoGpJEM7W/uTgbixNnbj8l6eFeKBU5opmeHq47dancbubmFw5h2e0gXuL27oKNYIQFZQYsvFiuEQVLck8McrfR7Qfa4vtp3XuRByXjzAp8Jg8ZhlTLExjm9SB8hfBZxsCxjwfNmyiwO2y3nAfH0M1K3xlZAypbfqteRCW3JyjLJa/ltqF6ucJ/smo1sT0g3fRzpWBp09yt7UcP00wb4inhmdlaCXxsfcgDW7hrsgScTUYaX+LgLQ1zrtlYdG72s7VRNGz9LyePrB08OhJdTRPJaJS258TKC0DKWkZRe2n193GnCuGxwsP6LZITI2zKdkUcjsoLjr5SW2BuL7Ln+C4RJPikVB06zwLAyeSmqJRaMAHJoCQ6IHKA0an8EFzQMdjc4qHhzcNgeDBGRbxUgH7WQf9NrvhHey6I1oWIYgxoa0ANAAAAsABsABsF+0BERAREQEREBERAREQEREEPxU62NYP/EK8fK0THf0VwobmbOGPw150IxCGx7C7Xg39iDZW4QaTi7DhNASaiWn6V5RLE6xbkBvcbOFr6EfmuY0WBYvi1LT1T6mna4OL4JHROZOxodYEGMZS14A8pBFgFbc2ah/gTTx2MtXJHSsB/8ANPmP4NB+SrqKnEbGxjZrWtHyaLD/ACQc6HGeK09VHRVFDDI94OSRs3TbUFvdmYEBx+4bFb3/AHhxT/wf/wB5Fb/QtvxRgMdbA6CS4vYse3R8b26skYexBU/heO1NC6OnxMsLXXbHWNdaN5GzZg4DpvI73IJ73QMR4zrqeF88+EuZHGC6Qiqhc4AdwLC4/NfT/fCtyteMGnc12WxbNTu+O1j5XE21vfYd15ObcrHU9LC5peyespmG2oLQS8ggbghv9VfAIOQcbYrW4m1mHHDKinbJPG2SWQ/ZkMJc4Xa0i3luHXN7LNZhFRQVbafCKxznvYHS00/2kcLG3PVc9zvJe4AaBc69hp19c54i5bSS1EslNVvhjq3DxrbjO5rbkCI5bi50IJAse+xDl/GHG1biTW0nRjmMT80nh2GSGUgjI4aZ2i926OsQSutcreBm4dDnkDXVEti5wDgY2ENIh8zjo0g66X09FT8OcP09DEIaeIMaPT4nH1cdyVtUBERAREQEREBERAREQEREBERBz3nhCP0Y6Ql14poXtyus4HOGmx7GzjY9jZcnwLnHiNP5XuZUMuLdUDOB6Zm2J+ZB1Xd+YUEcmG1jZAC0QSu17FjS5rhpuCAR8lN4Zy8oK3D6Mz0+WQU0XnYcj9Wh2pbo7e+t90HPanm94mqopZ6cxsppHyv6bsxeXNLW2BAta/qb6q/h524YW3d1mH7pjJJ9wQbafNQ0fKSKqrK6CnmdC2mfA1vUAkLuozO4m2W3t/8Arfmu5FTxNe81sHTa1zi4teDZovqNQPqg6bS82sIeWjxgaT2dFM0D+84syj62WxquI8LqIzHJVUssbrBzXPjc11jcAg6bi/zC/kUhX/DPKupr6RtVTzQEOJGRxeHAtNnAnKQLboKrjWrhw+ooI2VZqKJs4nDM7ZZIOl5SA69y0iTRrtshsu0YLjUFXGJaeVsjD3adj6HuD7FcDr+R1ZFFJJ14HZGPflbnzOLWk5RcAa27ldLwvhGjraeCtp81LM+CPLLTExWIaAMzAcrgHD4TfQWJQdBRQTq7GKG/WhbiEIb8cAEdQLaaxknMe+h7rd8NcZUlaSyGUiRts0UjSyRt9rtcNfwugokWLrKAiIgIiICIiAiIgIiICIiAiLBKCO5qEuoTAHlpqZoKe4+K0sjQ+2v3Afa11U0tM2KNsbdGsaGNHo1osPyCg8aqY6zGY4ZHgU+HtE02cgRmolH2AJO5DSTv6+63HMLG+nh0rqd4dJNangLHAl0sxyNDCD8QuT7Wv2QaDk/iAnnxZ+UXNa52YW8zDmDG6ACzQ3fvmK+3PPHxT4a6EOtJUnptGlywWdKbemWzSf4x6r78AYZHRVlTRxsDQ2monu2u59pGPcbbkkXJURzow6uxCvjggpZHxwNaGuDfKXzjM45jpYCMD+U3QcuoMEdLR1NWA77B8A0HlIlzhxJ9rN/7lb8heKDBW+Fe+0M4NgTZolFspF+5Ay27khW/Kvg2VmGVdLVxOjNQ97crrXDem1rXjfud/wCFTnC/JWtinimlnhjEb2v8l3v8jgRlzNDb6d9kHeXsDgQdiLEe3dQvJ1xZSS0jtXUlTPBfXUB2cEX2Hm0HpZXbT7qH4OrQMWxWm8oOeCcWdqQ6NrHaexaL/wB5BcObdTvE/BFHXeaVhbKPhmjOSZp7EOG9ve6pEQQLqHGqM2hmgroA3QVGZlTp2DmDKT2ufZe3CuYlM94hqGS0c2gyVLcgc7YiN17OF7gHS9lYWWm4tw+CWmkM9KKkMY57Y8oLy4NOjCfhcb2uPVB4MS4yZTVkdPUR9OGZl4qkub0nSDVzHfd0LbE7kqmjkDgCCCDqCDcH5LgdLhWLvhEdLhswpnE/q9XUQTQZW6WayVrJGWdYg5u2io+X1NjtJO2KWitROcBk68LvDg6XYTKXFo3y66bIOuIsBZQEREBERAREQEREBflzbr9Ig4pw7wDSyY1UsdmnjpGsMxncJDPLUtLm3vsGi41vqwK7j5aYcyeOoihMT45GygMe4RlzdrsJLQPkAvHwPH/auMv9ZaVu33Ij/wDZXiDneOYtJRYu57aOeo8RTRRx9FotnjfISHk2DRZ1730stjLxLiDjZmESNFiS+aoga1the5DC8/RWWVT/ADAxUUmHVM+lxGWtBFwXyeRgtf7zgglOGOL8UxKAy01NSMbnfHnfLIcrmgEnIGjMNRse62PisfsP1ahBy2I6smrzbz7aAW+Hvm30VJwlhvhqKmgtYxxRtN98waM3Yd7rbWQQ7qfH/wDr4cPlHUfTdTmB4NVUmOsqq50T3VjJY2OhDxG2RrWuyEO11Yw2v6LrimuO8EfVUw6RtNDIyoh3F5IrkNNtfM0lv8yCkBWVp+FMeZW0zKhnlzaPYfijkbo+N3uDotwgIiIMALKIgIiICIiAiIgIiICIiAsErK/LggjuAATPichaQH1z8pcCCQyNjToddDorNR3LZ+dlZKQMz66pDrd8jsg/IKxQFD80HtlZSUN/NVVUQA/ghcJJXetgAB/MFcFcq5g4JJiOJxxQuDH0dN4hpNy10j5B02EDsemb/gg6oFlaThLiBlbAJBZsrfLPFfzQyj42O76FbtAWCFlYKCGqqT9H4jA6BxENfNI2eH9wTZC/xDO4ccliNirpQuJTPqMcp4LfZUkDqknuZZc0TG37DKSfr7K6QEREBERAREQEREBERAREQEREBYJWVgoI/ley1NM7s+srHNI9Os5v+bSrFR/KYk4VTuP75nk/9SeR4v8Ag5WCAVG8G/a12KVTTdjpYIGH/DR5ZLW7dR7vp2VHjeKspYJaiU2ZG0udYXOnYW7k6fitHyww6SDDoer+1lzzy6WOedxksR6gED8EHi4l4RlbUfpDDXNjqrWljd+xqmjXK/0doPP8vmscHcy6WtlNM4OhqQXN6btQ4s+LI8aHY6Gx0Kp+IsQFNTT1DhcRRSSEeuVpNh7nb8VJcs+FYRR0VTLEDO1kkrHHdniT1CAPkRYm5AJ9SgvwVhyAIUERwQ8T12J1QubTtpWE2tamaMwFidM7ib22t7gXCjOVUY8HI+xBkq6t5v7zOA/IBWaAiIgIiICIiAiIgIiICIiAiIgLXcQTllNO8EAtikcCexDSQfrZbFaniuPNRVIvb7CX0+4T3BHb0QeHlwwDC6Gwt+rxH8S0En6kqkU7y7/4XQ/4aH/QFRIInmtH1aWOkzBviqmngce4aXZ3Eep8lvxVnGywsNhoPkNlHccP/XcJFt6t529IXj+qtEEbzbnc3DJ2NF3TGOBovbWZ7W/5FVGF0vShji+5Gxn/AGtDf6KT5neb9HwjUy4jSggOscjC573D5Bo+oVsgLwY/WiClnnN7RQyyG2/kYXafRe9R3NOsy0QhG9VPDS97kTOs8CxGpYHINpwLSGLDqRhvmEERdc3Od7Q99z/ecVvV+ImAAAbAAD5AL9oCIiAiIgIiICIiAiIgIiICIiAtDx3ViHD6qQ30gk2tfUZe/wA1vl5cTw+OoifDK3NHI0te3UXa7Qi41H4INLwZNHBhtEJJYwPDwgOzAMd5B8JNrr7z8ZYez4q6nGttZWb/AFXO+OuWmG00UT4achzqqmjI6krszJHhr2gF3pr6q+i4Hw5gIZRQNuLXEbc1iLXuRvrugmMXx6lrMXwpkFTHK1hqnuyPBs4Rjpg29fN9F0pc05N4TTCndaFnWpqqpizloMjbO9d/hIC6WggOZjzDUYdWPY91PTzvMxa0u6QeywlIF3ZW2ubA/D6kXpcN4toahueKrhcNP32gi4uAQbEH2Oq3JC5lX4BTVWPiKSmjdFFRGUjI3K6WSXKC/TzeW+/cIOhsxSAtc8TRlrBd5D2kNGuriDpsd/RRDcRgxPFoWRvjnp6SA1GZrg5niHvDI9jbM1rSdfVbHEeXeGEGQUEJexj8ga2wJI0BaLNcdNMwNlruSbaf9HR9MME4zNqLACTOHGwk73sR9UHQQsoiAiIgIiICIiAiIgIiICIiAiIgL8vK/SmOYuJPhopBDYzzZYIG31c+ZwZp8gS72AKDRYLA/Fa3x0j3eDppSKNgDmiV7QA6odr5rOBA09fe/QXvaASSABuTawt6rT07KfDaJrXObHDTxAE62s0XJ7kkm5tqSSo0Mq8ccCc1Lhl/h169Y3QjNtkjcP679gzw7VMdjtQaDz0r4/1wtP2Iqbmz2di82ANty4k3K6Ytfg2DQUsYip4mxxjXK0aXO5Pcn3K2CAoHDsUgZjVaZpmwu6NNFG2VwZ1AMz3PjzGzhd1vmHK+Wrxfh6lqwBU08ctts7QSPkdwg2LJGu1BBHsbqPx/gvNI6roZfCVjvikAuyZoB8kjL5dTlJda/lXwn5bMiJdh1VNQF1szYznicR+85jzqbG1wQvMHcQUzQzJS1w2D83SeL/vPGjSB7aoPvS8dyU5bHilK6lv5RODnp3uBAvdoJYDe4zK7BXKOJoMbxOF1FJh0NPG9zc03iGOsI3B4sGknXL6d+y6rAzK1rfQAfQWQftERAREQEREBERAREQEREBERBgrnfMKvkgrqKd9NPLTwNnkBhbmzTuaWsa4btAYHG+3m9l0VfOZBzxnDdXiszJ8RHSpGnNHQ65jtldOQbE3F8tjvbRdDijDQGgAAAAACwAGwHsv21ZQEREBERAWLLKIMWWURAREQEREBERAREQEREH//2Q=='),
    ('Fumi Kiri Geri', 'coup de pied coupant', 'http://skdmery.e-monsite.com/medias/images/fumi-kiri-geri.jpg');

INSERT INTO mouvements.sous_categorie (nom, categorie_parent) VALUES 
    ('projections', 'Déplacement'),
    ('poings', 'Techniques de bras'),
    ('coudes', 'Techniques de bras'),
    ('pieds', 'Attaques de pieds'),
    ('genoux', 'Attaques de pieds');

INSERT INTO mouvements.sous_categorie_mouvement (mouvement, sous_cat) VALUES
    ('Ayumi Ashi', 'projections'),
    ('Age Zuki', 'poings'),
    ('Jodan Mae Age Empi Uchi', 'coudes'),
    ('Jodan Mae Age Empi Uchi', 'poings'),
    ('Oi Zuki', 'poings'),
    ('Furi Uchi', 'poings'),
    ('Fumi Kiri Geri', 'pieds');


INSERT INTO public.club (nom, site_web, adresse_dirigeant) VALUES 
    ('Fédération Française de Karaté et Disciplines Associées', 'https://www.ffkarate.fr/', '39 rue Barbès, 92120 Montrouge'),
    ('CSAHR', 'https://www.csahr.com/rueil/karate/', '81 rue des bons raisins 92500 Rueil-Malmaison'),
    ('Karaté Club Colombes', 'https://www.karatecolombes.fr/', 'Gymnase Bienvetu, 77 Avenue Henri Barbusse, 92700 Colombes'),
    ('Club Karaté Anthony', 'http://www.club-karate-antony.com/', '10 Bis r Paul Cézanne, 92160 ANTONY'),
    ('Karate Club Tonnay Charente', 'http://www.kctc.fr/', 'Complexe Municipal, 17430 Tonnay-Charente');

INSERT INTO public.professeur_club (karateka, club) VALUES 
    ('FRA2115', 'Fédération Française de Karaté et Disciplines Associées'),
    ('CGO136', 'Fédération Française de Karaté et Disciplines Associées'),
    ('FRA2284', 'Fédération Française de Karaté et Disciplines Associées'),
    ('FRA2059', 'Fédération Française de Karaté et Disciplines Associées'),
    ('CGO136', 'CSAHR'),
    ('FRA2059', 'CSAHR'),
    ('FRA2115', 'Karaté Club Colombes'),
    ('FRA2059', 'Karaté Club Colombes'); 


INSERT INTO public.competition_mixte (reference, nom, debut, fin, lieu, site_web, mail_contact, club) VALUES 
    ('FRA-B-2020', 'Coupe de France Combats Benjamains', to_date('30-05-2020', 'DD-MM-YYYY'), to_date('31-05-2020', 'DD-MM-YYYY'), 'Amiens (80)', 'https://www.ffkarate.fr/calendrier/coupe-de-france-combats-benjamins-m-f/', NULL, 'Fédération Française de Karaté et Disciplines Associées'),
    ('TQO-2021', 'Tournoi Qualification Olympique', to_date('08-05-2020', 'DD-MM-YYYY'), to_date('10-05-2020', 'DD-MM-YYYY'), 'AccorHotels Arena (75)', 'https://www.francekarate2020.fr/fr/tokyo-2020/agenda/2020-05-08/tournoi-de-qualification-2020-partie-2', 'qualifications.tokyo@ffk.com', 'Fédération Française de Karaté et Disciplines Associées');

INSERT INTO public.posseder_image_competition_mixte(competition, image) VALUES 
    ('TQO-2021', 'https://www.francekarate2020.fr/medias/articles/desktop-systeme-de-qualification-picture-20180903123857.jpg'),
    ('TQO-2021', 'https://www.pepsup.com/resources/images/ARTICLES/000/023/404/234044/IMAGE/234044.jpg?1574039461000'),
    ('TQO-2021', 'https://www.supersportci.net/fr/wordpress/wp-content/uploads/2020/04/fce11964a1cbdab65fa5f06b1c9e8467.jpg'),
    ('FRA-B-2020', 'https://www.ffkarate.fr/wp-content/uploads/2015/05/CoupeFrBenjamins2014-scaled.jpg'),
    ('FRA-B-2020', 'https://sites.ffkarate.fr/hautsdeseine/wp-content/uploads/sites/4/2019/05/20190420-21_CPE_FRANCE_BENJAMINS_COMBAT_Nelisa_CSK_3eme.jpg');

INSERT INTO public.autoriser_coup_kumite_mixte(competition, sous_categorie, points) VALUES 
    ('TQO-2021', 'poings', 10),
    ('TQO-2021', 'coudes', 4),
    ('TQO-2021', 'projections', 6),
    ('FRA-B-2020', 'pieds', 5);

INSERT INTO public.interdire_coup_kumite_mixte(competition, sous_categorie) VALUES 
    ('TQO-2021', 'pieds'),
    ('FRA-B-2020', 'coudes'),
    ('FRA-B-2020', 'poings');


INSERT INTO kata.famille_kata(nom_japonais, nom_francais) VALUES 
    ('Heian', 'Paix tranquille'),
    ('Taikyoku', 'Cause ultime');

INSERT INTO kata.kata (nom_japonais, nom_francais, ceinture, nb_dans, schema_kata, famille_kata, description) VALUES
    ('Heian Shodan', 'paix et tranquillité, niveau un', 'jaune', 2, 'https://i.pinimg.com/originals/fa/72/96/fa72967a066d20d4584947eccde28c81.jpg', 'Heian',  'Heian Shodan est la base de la pratique du karaté et est en général le premier kata qu''un karatéka apprend et pratique'),
    ('Heian Nidan', 'paix et tranquillité, niveau deux', 'orange', 4, 'https://i.pinimg.com/originals/8a/16/4c/8a164cd08992299efa3f777a57419c2c.jpg', 'Heian',  'Le kata se compose de 26 mouvements et s''exécute en environ 40 secondes. Ce kata contient, comparativement à Heian Shodan, des techniques plus difficiles comme Yoko Geri (coup de pied latéral) et Mae Geri (coup de pied frontal), Nukite (frappe du bout des doigts) et Uchi Uke (technique de blocage d''intérieur en extérieur). Un élément central de ce kata est la position vers l''arrière appelée Kokutsu Dachi qui y est particulièrement fréquente.'),
    ('Heian Sandan', 'paix et tranquillité, niveau trois', 'verte', 7, 'https://i.pinimg.com/originals/2d/b4/74/2db4742c790429e6d93d56fb4063f27d.png', 'Heian',  'Heian Sandan suit directement le kata Heian Nidan dans l''apprentissage et contient des techniques basses et moyennes.'),
    ('Heian Yondan', 'paix et tranquillité, niveau quatre', 'noire', 15, 'https://budokai.ca/images/Heian_Yondan.jpg', 'Heian',  'Ce kata est caractérisé par une grande dynamique, l'>'étude de techniques doubles de blocage et l''utilisation de techniques de pieds. Celui-ci fut fort probablement dérivé du kata Kushanku (ou Kanku). Il se compose de 27 actions et s''exécute en environ 50 secondes.'),
    ('Taikyoku shodan', 'Cause ultime, niveau un', 'jaune', 3, 'https://s2.static-clubeo.com/uploads/toulousekarate/news/taikyoku-shodan__nkjssm.jpg', 'Taikyoku', 'Taikyoku Shodan, often simply referred to as "kihon" is the first of the series, and involves only two basic moves: the gedan barai or low block, and chudan (middle) oi zuki (sometimes "oi tsuki"), or lunge punch.'),
    ('Taikyoku nidan', 'Cause ultime, niveau deux', 'bleue 2', 5, 'https://espritbubishi.files.wordpress.com/2011/02/katataykyokunidan.gif', 'Taikyoku', '- Déplacement du pied gauche d''un demi-pas vers le pied droit- Amener le pied droit contre le pied gauche en position Musubi-Dachi'),
    ('Taikyoku sandan', 'Cause ultime, niveau trois', 'marron 1', 10, 'https://tcmskarate.fr/wp-content/uploads/2013/10/taiktoku-sandan.png', 'Taikyoku', 'Taikyoku Shodan, often simply referred to as "kihon" is the first of the series, and involves only two basic moves: the gedan barai or low block, and chudan (middle) oi zuki (sometimes "oi tsuki"), or lunge punch.');

INSERT INTO kata.presenter_kata (nom_kata, video_kata) VALUES 
    ('Heian Shodan', 'https://encrypted-vtbn2.gstatic.com/video?q=tbn:ANd9GcT_ncMD3cV-6QXwJ37IrXnWcuiR9pM-FC5vABCFxN69KwVPOSK_'),
    ('Heian Nidan', 'https://www.youtube.com/watch?v=nSUMlMwnPG8'),
    ('Heian Sandan', 'https://www.youtube.com/watch?v=8OI-FTV3-4o'),
    ('Heian Yondan', 'https://www.youtube.com/watch?v=HrAiTCcatbY'),
    ('Taikyoku shodan', 'https://www.youtube.com/watch?v=PDONkxaZk7A'),
    ('Taikyoku nidan', 'https://www.youtube.com/watch?v=aL6F6AWjguc'),
    ('Taikyoku nidan', 'http://www.karate-tourny27.fr/?module=KTNID'),
    ('Taikyoku sandan', 'https://www.youtube.com/watch?v=H47PYxqzFFs'),
    ('Taikyoku sandan', 'https://www.youtube.com/watch?v=bf4YAhdEMlc');

INSERT INTO kata.competition_kata (reference, nom, debut, fin, lieu, site_web, mail_contact, club) VALUES
    ('KATA_FR_100', 'Compétition Heian', to_date('24-08-2020', 'DD-MM-YYYY'), to_date('30-08-2020', 'DD-MM-YYYY'), '10 Bis r Paul Cézanne, 92160 ANTONY', 'http://www.club-karate-antony.com/competition/competition-kata/', 'shotokankarateantony@gmail.com', 'Club Karaté Anthony'),
    ('KATA_FR_101', 'Compétition Sandan', to_date('15-01-2020', 'DD-MM-YYYY'), to_date('25-01-2020', 'DD-MM-YYYY'), '81 rue des bons raisins 92500 Rueil-Malmaison', 'https://www.csahr.com/rueil/karate/', 'infos@csahr.com', 'CSAHR');


INSERT INTO kata.posseder_image_kata (competition, image) VALUES 
    ('KATA_FR_100', 'https://media.gettyimages.com/photos/sandra-sanchez-jaime-of-spain-competes-in-the-womens-karate-kata-picture-id477076216?s=612x612'),
    ('KATA_FR_100', 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTkf7-AiAMU09c1wDuO5dp9JHDXLBsEHmkULYP1D5807N-3ijJt&usqp=CAU'),
    ('KATA_FR_101', 'https://www.csahr.com/rueil/wp-content/themes/simplecorp/library/images/disciplines/KAR_03.jpg');

INSERT INTO kata.confrontation_kata (competition, gagnant, perdant, score_gagnant, score_perdant, date_confrontation, numero_salle, adresse_salle, tour, kata) VALUES
    ('KATA_FR_100', 'FRA2115', 'FRA2059', 50, 45, to_timestamp('25-08-2020 09:00', 'DD-MM-YYYY HH24-MI'), 'B10', '10 Bis r Paul Cézanne, 92160 ANTONY', 'demi', 'Heian Sandan'),
    ('KATA_FR_100', 'CGO136', 'FRA2284', 35, 28, to_timestamp('28-08-2020 14:00', 'DD-MM-YYYY HH24-MI'), 'B10', '10 Bis r Paul Cézanne, 92160 ANTONY', 'demi', 'Heian Sandan'),
    ('KATA_FR_100', 'FRA2115', 'CGO136', 40, 37, to_timestamp('30-08-2020 16:00', 'DD-MM-YYYY HH24-MI'), 'C15', '10 Bis r Paul Cézanne, 92160 ANTONY', 'finale', 'Heian Yondan'),
    ('KATA_FR_101', 'CGO136', 'FRA2115', 28, 15, to_timestamp('28-01-2020 10:00', 'DD-MM-YYYY HH24-MI'), 'A01', '84 rue des bons raisins 92500 Rueil-Malmaison', 'finale', 'Taikyoku sandan');

INSERT INTO kata.confrontation_kata_mixte (competition, gagnant, perdant, score_gagnant, score_perdant, date_confrontation, numero_salle, adresse_salle, tour, kata) VALUES
    ('TQO-2021', 'FRA2284', 'FRA2115', 9, 8, to_timestamp('09-05-2020 11:00', 'DD-MM-YYYY HH24-MI'), 'ROOM_05', 'AccorHotels Arena (75)', 'qualif', 'Heian Yondan');


INSERT INTO kata.maitrise_kata (karateka, nom_kata) VALUES 
    ('FRA2115', 'Heian Shodan'),
    ('FRA2115', 'Heian Nidan'),
    ('FRA2115', 'Heian Sandan'),
    ('CGO136', 'Taikyoku nidan'),
    ('CGO136', 'Taikyoku sandan'),
    ('FRA2284', 'Taikyoku nidan');


INSERT INTO kata.mouvement_kata (nom_kata, mouvement, position) VALUES 
    ('Heian Shodan', 'Ayumi Ashi', 1),
    ('Heian Nidan', 'Age Zuki', 1),
    ('Heian Sandan', 'Oi Zuki', 1),
    ('Heian Yondan', 'Ayumi Ashi', 1),
    ('Heian Yondan', 'Oi Zuki', 2),
    ('Taikyoku shodan', 'Furi Uchi', 1),
    ('Taikyoku nidan', 'Fumi Kiri Geri', 1),
    ('Taikyoku sandan', 'Furi Uchi', 1),
    ('Taikyoku sandan', 'Fumi Kiri Geri', 3);



INSERT INTO kumite.competition_kumite (reference, nom, debut, fin, lieu, site_web, mail_contact, club) VALUES
    ('KUMITE_ANTHONY_2020', 'Compétition KUMITE à Anthony', to_date('14-04-2018', 'DD-MM-YYYY'), to_date('30-04-2018', 'DD-MM-YYYY'), '10 Bis r Paul Cézanne, 92160 ANTONY', 'http://www.club-karate-antony.com/competition/competition-kumite/', 'shotokankarateantony@gmail.com', 'Club Karaté Anthony');


INSERT INTO kumite.posseder_image_kumite (competition, image) VALUES 
    ('KUMITE_ANTHONY_2020', 'https://i.ytimg.com/vi/29xXDw2lsVk/maxresdefault.jpg'),
    ('KUMITE_ANTHONY_2020', 'https://www.gravelineskarateclub.com/media/uploaded/sites/5504/actualite/569ffb4d8d219_574.jpeg');

INSERT INTO kumite.confrontation_kumite (competition, participant1, participant2, date_confrontation, numero_salle, adresse_salle, tour) VALUES
    ('KUMITE_ANTHONY_2020', 'FRA2059', 'CGO136', to_date('17-04-2018 14:15', 'DD-MM-YYYY HH24-MI'), 'A15', '10 Bis r Paul Cézanne, 92160 ANTONY', 'demi'),
    ('KUMITE_ANTHONY_2020', 'FRA2115', 'FRA2284', to_date('18-04-2018 15:00', 'DD-MM-YYYY HH24-MI'), 'A15', '10 Bis r Paul Cézanne, 92160 ANTONY', 'demi'),
    ('KUMITE_ANTHONY_2020', 'FRA2059', 'FRA2115', to_date('19-04-2018 10:45', 'DD-MM-YYYY HH24-MI'), 'D20', '10 Bis r Paul Cézanne, 92160 ANTONY', 'finale');


INSERT INTO kumite.confrontation_kumite_mixte (competition, participant1, participant2, date_confrontation, numero_salle, adresse_salle, tour) VALUES
    ('TQO-2021', 'CGO136', 'FRA2059', to_date('09-05-2020 12:30', 'DD-MM-YYYY HH24-MI'), 'ROOM_03', 'AccorHotels Arena (75)', 'qualif');


INSERT INTO kumite.autoriser_coup_kumite (competition, sous_categorie, points) VALUES 
    ('KUMITE_ANTHONY_2020', 'poings', 1),
    ('KUMITE_ANTHONY_2020', 'coudes', 2);

INSERT INTO kumite.interdire_coup_kumite (competition, sous_categorie) VALUES 
    ('KUMITE_ANTHONY_2020', 'pieds'),
    ('KUMITE_ANTHONY_2020', 'genoux');

INSERT INTO kumite.coup_kumite (id, participant1, participant2, competition, sous_cat, source) VALUES 
    (1, 'FRA2059', 'CGO136', 'KUMITE_ANTHONY_2020', 'poings', 'FRA2059'),
    (2, 'FRA2059', 'CGO136', 'KUMITE_ANTHONY_2020', 'poings', 'FRA2059'),
    (3, 'FRA2059', 'CGO136', 'KUMITE_ANTHONY_2020', 'coudes', 'FRA2059'),
    (4, 'FRA2059', 'CGO136', 'KUMITE_ANTHONY_2020', 'poings', 'CGO136'),
    (5, 'FRA2115', 'FRA2284', 'KUMITE_ANTHONY_2020', 'coudes', 'FRA2115'),
    (6, 'FRA2115', 'FRA2284', 'KUMITE_ANTHONY_2020', 'poings', 'FRA2115'),
    (7, 'FRA2115', 'FRA2284', 'KUMITE_ANTHONY_2020', 'coudes', 'FRA2284'),
    (8, 'FRA2059', 'FRA2115', 'KUMITE_ANTHONY_2020', 'coudes', 'FRA2059');

INSERT INTO kumite.coup_kumite_mixte (id, participant1, participant2, competition, sous_cat, source) VALUES 
    (1, 'CGO136', 'FRA2059', 'TQO-2021', 'projections', 'CGO136'),
    (2, 'CGO136', 'FRA2059', 'TQO-2021', 'coudes', 'CGO136'); 


INSERT INTO tameshi_wari.competition_tameshi_wari (reference, nom, debut, fin, lieu, site_web, mail_contact, club) VALUES
    ('TAMESHI_FR_100', 'Compétition de casse des planches', to_date('08-10-2020', 'DD-MM-YYYY'), to_date('08-10-2020', 'DD-MM-YYYY'), 'Complexe Municipal, 17430 Tonnay-Charente', 'http://www.kctc.fr/pages/le-karate/tameshiwari-le-test-de-casse.html', 'bureau.kctc@outlook.fr', 'Karate Club Tonnay Charente');


INSERT INTO tameshi_wari.posseder_image_tameshi_wari (competition, image) VALUES 
    ('TAMESHI_FR_100', 'https://i.pinimg.com/originals/97/82/32/978232c4d70c277ae87efb2bd1c5b746.jpg'),
    ('TAMESHI_FR_100', 'https://lh3.googleusercontent.com/proxy/9VLkOyQKAzgOvjREFHUFdurlVwctKse4BA387ndgeXFARqOyiPchIik1Ujx2EdZ3_BLuBdEvg-ZBfAWbPqWPfGR2fn1e9FcAuvPpEjSwvbWmxQ'),
    ('TAMESHI_FR_100', 'https://i.ytimg.com/vi/KyfeswHOxLg/maxresdefault.jpg');

INSERT INTO tameshi_wari.confrontation_tameshi_wari (competition, gagnant, perdant, score_gagnant, score_perdant, date_confrontation, numero_salle, adresse_salle, tour) VALUES
    ('TAMESHI_FR_100', 'FRA2059', 'FRA2115', 15, 10, to_timestamp('08-10-2020 09:00', 'DD-MM-YYYY HH24-MI'), 'HALL', 'Complexe Municipal, 17430 Tonnay-Charente', 'demi'),
    ('TAMESHI_FR_100', 'CGO136', 'FRA2284', 7, 3, to_timestamp('08-10-2020 12:00', 'DD-MM-YYYY HH24-MI'), 'HALL', 'Complexe Municipal, 17430 Tonnay-Charente', 'demi'),
    ('TAMESHI_FR_100', 'FRA2059', 'CGO136', 10, 3, to_timestamp('08-10-2020 15:00', 'DD-MM-YYYY HH24-MI'), 'HALL', 'Complexe Municipal, 17430 Tonnay-Charente', '3emep');
    
INSERT INTO tameshi_wari.confrontation_tameshi_wari_mixte (competition, gagnant, perdant, score_gagnant, score_perdant, date_confrontation, numero_salle, adresse_salle, tour) VALUES
    ('TQO-2021', 'CGO136', 'FRA2284', 9, 8, to_timestamp('10-05-2020 11:00', 'DD-MM-YYYY HH24-MI'), 'COUR', 'AccorHotels Arena (75)', 'finale');

INSERT INTO tameshi_wari.action_tameshi_wari (action) VALUES 
    ('Planches de casse'),
    ('PLANCHES DE SAPIN'),
    ('SEIKEN');

INSERT INTO tameshi_wari.evaluer_action_tameshi_wari (competition, perdant, action) VALUES
    ('TAMESHI_FR_100', 'FRA2115', 'PLANCHES DE SAPIN'),
    ('TAMESHI_FR_100', 'FRA2115', 'SEIKEN'),
    ('TAMESHI_FR_100', 'FRA2284', 'Planches de casse'),
    ('TAMESHI_FR_100', 'CGO136', 'Planches de casse'),
    ('TAMESHI_FR_100', 'CGO136', 'PLANCHES DE SAPIN');

INSERT INTO tameshi_wari.evaluer_action_tameshi_wari_mixte (competition, perdant, action) VALUES
    ('TQO-2021', 'FRA2284', 'SEIKEN');




COMMIT;