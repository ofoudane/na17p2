



CREATE TYPE COULEUR_CEINTURE AS ENUM ('jaune', 'orange', 'verte', 'bleue 1', 'bleue 2', 'marron 1', 'marron 2', 'marron 3', 'noire');
CREATE TYPE TOUR AS ENUM ('qualif', 'quart', 'demi', '3emep', 'finale');

BEGIN TRANSACTION;

CREATE TABLE public.karateka (
    reference varchar primary key,
    nom varchar not null,
    prenom varchar not null,
    poids int not null check (poids >= 10 and poids <= 1000),
    taille int not null check (taille >= 30 and taille <= 300),
    date_naissance Date not null,
    ceinture COULEUR_CEINTURE not null,
    photo varchar,
    unique (nom, prenom, date_naissance, poids),
    unique (nom, prenom, date_naissance, taille)
);



----------------- CLUB ----------------------
CREATE TABLE public.club (
    nom varchar primary key,
    site_web varchar,
    adresse_dirigeant varchar not null
);

CREATE TABLE public.professeur_club (
    karateka varchar references public.karateka (reference),
    club varchar references public.club(nom),
    primary key (karateka, club)
);


-------------- MOUVEMENTS ----------------
CREATE SCHEMA mouvements;
CREATE TABLE mouvements.mouvement (
    nom_japonais varchar primary key,
    nom_francais varchar unique not null,
    schema_mouvement varchar not null
);

CREATE TABLE mouvements.sous_categorie (
    nom varchar primary key,
    categorie_parent varchar not null
);

CREATE TABLE mouvements.sous_categorie_mouvement (
    mouvement varchar references mouvements.mouvement(nom_japonais),
    sous_cat varchar references mouvements.sous_categorie(nom),
    primary key(mouvement, sous_cat)
);



----------------- COMPETITION MIXTE --------------

CREATE TABLE public.competition_mixte (
    reference varchar primary key,
    nom varchar not null,
    debut Date not null,
    fin Date not null,
    lieu varchar not null,
    site_web varchar,
    mail_contact varchar,
    club varchar not null references public.club(nom),
    check (debut <= fin),
    unique (nom, debut),
    unique (nom, fin)
);

CREATE TABLE public.posseder_image_competition_mixte (
    competition varchar references public.competition_mixte(reference),
    image varchar,
    primary key (image, competition)
);

CREATE TABLE public.autoriser_coup_kumite_mixte (
    competition varchar references public.competition_mixte,
    sous_categorie varchar references mouvements.sous_categorie(nom),
    points int not null check (points > 0),
    primary key (competition, sous_categorie)
);

CREATE TABLE public.interdire_coup_kumite_mixte (
    competition varchar references public.competition_mixte,
    sous_categorie varchar references mouvements.sous_categorie(nom),
    primary key (competition, sous_categorie)
);


----------------- KATA ----------------------
CREATE SCHEMA kata;

CREATE TABLE kata.famille_kata (
    nom_japonais varchar primary key,
    nom_francais varchar not null unique
);

CREATE TABLE kata.kata (
    nom_japonais varchar primary key,
    nom_francais varchar unique not null,
    description varchar not null,
    ceinture COULEUR_CEINTURE not null,
    nb_dans int not null check (nb_dans > 0),
    schema_kata varchar not null,
    famille_kata varchar references kata.famille_kata(nom_japonais)
);

CREATE TABLE kata.presenter_kata (
    nom_kata varchar references kata.kata(nom_japonais),
    video_kata varchar,
    primary key (nom_kata, video_kata)
);

CREATE TABLE kata.competition_kata (
    reference varchar primary key,
    nom varchar not null,
    debut Date not null,
    fin Date not null,
    lieu varchar not null,
    site_web varchar,
    mail_contact varchar,
    club varchar references public.club(nom),
    check (debut <= fin),
    unique(nom, debut),
    unique(nom, fin)
);

CREATE TABLE kata.posseder_image_kata (
    competition varchar references kata.competition_kata(reference),
    image varchar,
    primary key (image, competition)
);

CREATE TABLE kata.confrontation_kata (
    perdant varchar not null references public.karateka(reference),
    competition varchar references kata.competition_kata(reference),
    gagnant varchar not null references public.karateka(reference),
    score_gagnant int not null check (score_gagnant > 0),
    score_perdant int not null check (score_perdant >= 0),
    date_confrontation timestamp not null,
    numero_salle varchar not null,
    adresse_salle varchar not null,
    tour TOUR not null,
    kata varchar not null references kata.kata(nom_japonais),
    primary key(perdant, competition),
    check (score_gagnant > score_perdant),
    check (gagnant != perdant)
);

CREATE TABLE kata.confrontation_kata_mixte (
    perdant varchar references public.karateka(reference),
    competition varchar references public.competition_mixte(reference),
    gagnant varchar not null references public.karateka(reference),
    score_gagnant int not null check (score_gagnant > 0),
    score_perdant int not null check (score_perdant >= 0),
    date_confrontation timestamp not null,
    numero_salle varchar not null,
    adresse_salle varchar not null,
    tour TOUR not null,
    kata varchar not null references kata.kata(nom_japonais),
    primary key(perdant, competition),
    check (score_gagnant > score_perdant),
    check (gagnant != perdant)
);


CREATE TABLE kata.maitrise_kata (
    nom_kata varchar references kata.kata(nom_japonais),
    karateka varchar references public.karateka(reference),
    primary key(nom_kata, karateka)
);

CREATE TABLE kata.mouvement_kata (
    nom_kata varchar references kata.kata(nom_japonais),
    mouvement varchar references mouvements.mouvement(nom_japonais),
    position int not null check (position > 0),
    primary key (nom_kata, position),
    unique (nom_kata, mouvement)
);



----------------- KUMITE ----------------------
CREATE SCHEMA kumite;

CREATE TABLE kumite.competition_kumite (
    reference varchar primary key,
    nom varchar not null,
    debut Date not null,
    fin Date not null,
    lieu varchar not null,
    site_web varchar,
    mail_contact varchar,
    club varchar references public.club(nom),
    check (debut <= fin),
    unique (nom, debut),
    unique (nom, fin)
);

CREATE TABLE kumite.posseder_image_kumite (
    competition varchar references kumite.competition_kumite(reference),
    image varchar,
    primary key (image, competition)
);

CREATE TABLE kumite.confrontation_kumite (
    participant1 varchar references public.karateka(reference),
    participant2 varchar references public.karateka(reference),
    competition varchar references kumite.competition_kumite(reference),
    date_confrontation timestamp not null,
    numero_salle varchar not null,
    adresse_salle varchar not null,
    tour TOUR not null,
    primary key (participant1, participant2, competition),
    check (participant1 != participant2)
);

CREATE TABLE kumite.confrontation_kumite_mixte (
    participant1 varchar not null references public.karateka(reference),
    participant2 varchar not null references public.karateka(reference),
    competition varchar references public.competition_mixte(reference),
    date_confrontation timestamp not null,
    numero_salle varchar not null,
    adresse_salle varchar not null,
    tour TOUR not null,
    primary key (participant1, participant2, competition),
    check (participant1 != participant2)
);


CREATE TABLE kumite.coup_kumite (
    id int primary key,
    participant1 varchar not null,
    participant2 varchar not null,
    competition varchar not null,
    source varchar not null references public.karateka(reference),
    sous_cat varchar not null references mouvements.sous_categorie(nom),
    check (source = participant1 or source = participant2),
    foreign key (participant1, participant2, competition) references kumite.confrontation_kumite(participant1, participant2, competition)
);

CREATE TABLE kumite.coup_kumite_mixte (
    id int primary key,
    participant1 varchar not null,
    participant2 varchar not null,
    competition varchar not null,
    source varchar not null references public.karateka(reference),
    sous_cat varchar not null references mouvements.sous_categorie(nom),
    check (source = participant1 or source = participant2),
    foreign key (participant1, participant2, competition) references kumite.confrontation_kumite_mixte(participant1, participant2, competition)
);


CREATE TABLE kumite.autoriser_coup_kumite (
    competition varchar references kumite.competition_kumite(reference),
    sous_categorie varchar references mouvements.sous_categorie(nom),
    points int not null check (points > 0),
    primary key (competition, sous_categorie)
);

CREATE TABLE kumite.interdire_coup_kumite (
    competition varchar references kumite.competition_kumite(reference),
    sous_categorie varchar references mouvements.sous_categorie(nom),
    primary key (competition, sous_categorie)
);



----------------- TAMESHI_WARI ----------------------

CREATE SCHEMA tameshi_wari;

CREATE TABLE tameshi_wari.competition_tameshi_wari (
    reference varchar primary key,
    nom varchar not null,
    debut Date not null,
    fin Date not null,
    lieu varchar not null,
    site_web varchar,
    mail_contact varchar,
    club varchar references public.club(nom),
    check (debut <= fin),
    unique (nom, debut),
    unique (nom, fin)
);

CREATE TABLE tameshi_wari.posseder_image_tameshi_wari (
    competition varchar references tameshi_wari.competition_tameshi_wari(reference),
    image varchar,
    primary key (image, competition)
);

CREATE TABLE tameshi_wari.confrontation_tameshi_wari (
    perdant varchar references public.karateka(reference),
    competition varchar references tameshi_wari.competition_tameshi_wari(reference),
    gagnant varchar not null references public.karateka(reference),
    score_gagnant int not null check (score_gagnant > 0),
    score_perdant int not null check (score_perdant >= 0),
    date_confrontation timestamp not null,
    numero_salle varchar not null,
    adresse_salle varchar not null,
    tour TOUR not null,
    primary key(perdant, competition),
    check (score_gagnant > score_perdant),
    check (gagnant != perdant)
);

CREATE TABLE tameshi_wari.confrontation_tameshi_wari_mixte (
    perdant varchar references public.karateka(reference),
    competition varchar references public.competition_mixte(reference),
    gagnant varchar not null references public.karateka(reference),
    score_gagnant int not null check (score_gagnant > 0),
    score_perdant int not null check (score_perdant >= 0),
    date_confrontation timestamp not null,
    numero_salle varchar not null,
    adresse_salle varchar not null,
    tour TOUR not null,
    primary key(perdant, competition),
    check (score_gagnant > score_perdant),
    check (gagnant != perdant)
);

CREATE TABLE tameshi_wari.action_tameshi_wari (
    action varchar primary key
);

CREATE TABLE tameshi_wari.evaluer_action_tameshi_wari (
    perdant varchar,
    competition varchar,
    action varchar references tameshi_wari.action_tameshi_wari(action),
    primary key(perdant, competition, action),
    foreign key (perdant, competition) references tameshi_wari.confrontation_tameshi_wari(perdant, competition)
);

CREATE TABLE tameshi_wari.evaluer_action_tameshi_wari_mixte (
    perdant varchar,
    competition varchar,
    action varchar references tameshi_wari.action_tameshi_wari(action),
    primary key(perdant, competition, action),
    foreign key (perdant, competition) references tameshi_wari.confrontation_tameshi_wari_mixte(perdant, competition)
);


COMMIT;