BEGIN TRANSACTION;

DROP SCHEMA kata CASCADE;
DROP SCHEMA tameshi_wari CASCADE;
DROP SCHEMA kumite CASCADE;
DROP SCHEMA mouvements CASCADE;
DROP TABLE public.karateka CASCADE;
DROP TABLE public.competition_mixte CASCADE;
DROP TABLE public.posseder_image_competition_mixte CASCADE;
DROP TABLE public.autoriser_coup_kumite_mixte CASCADE;
DROP TABLE public.interdire_coup_kumite_mixte CASCADE;
DROP TABLE public.club CASCADE;
DROP TABLE public.professeur_club CASCADE; 

COMMIT;