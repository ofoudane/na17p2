BEGIN TRANSACTION;

--Récupérer toutes les compétitions, que ce soit des compétitions mixtes, de kata, de kumite ou de tameshi wari.
CREATE OR REPLACE VIEW public.TOUTES_LES_COMPETITIONS AS 
    SELECT reference, nom, debut, fin, lieu, site_web, mail_contact, club, 'mixte' AS type FROM public.competition_mixte
    UNION
    SELECT reference, nom, debut, fin, lieu, site_web, mail_contact, club, 'kata' AS type FROM kata.competition_kata
    UNION
    SELECT reference, nom, debut, fin, lieu, site_web, mail_contact, club, 'kumite' AS type FROM kumite.competition_kumite
    UNION
    SELECT reference, nom, debut, fin, lieu, site_web, mail_contact, club, 'tameshi_wari' AS type FROM tameshi_wari.competition_tameshi_wari;

-- Récupérer le nom, début et fin des compétitions de kata ou de kumite.
SELECT nom, debut, fin from TOUTES_LES_COMPETITIONS WHERE type = 'kata' OR type = 'kumite';

--Pour chaque club, récupérer la liste des compétitions organisées par ce club.
CREATE OR REPLACE VIEW public.COMPETITIONS_CLUB AS
    SELECT comp.reference, comp.nom, comp.debut, comp.fin, comp.lieu, comp.site_web, comp.mail_contact, comp.type, club.nom AS nom_club, club.site_web AS site_club, club.adresse_dirigeant,
    CASE WHEN comp.debut > now() then True ELSE False END AS competition_future
        FROM public.TOUTES_LES_COMPETITIONS AS comp INNER JOIN public.club 
            ON (club.nom = comp.club);

--Lister le nom, le type, la date de début et de fin de chaque compétition passées, organisées par le club 'Fédération Française de Karaté et Disciplines Associées'.
SELECT nom, type, debut, fin FROM public.COMPETITIONS_CLUB WHERE nom_club = 'Fédération Française de Karaté et Disciplines Associées' AND not competition_future ;

--Lister le nom, le type, la date de début et le site_web de chaque compétition qui sera organisée par le club 'Club Karaté Anthony'
SELECT nom, type, debut, site_web FROM public.COMPETITIONS_CLUB WHERE nom_club = 'Club Karaté Anthony' AND competition_future;

--Récupérer les informations personnelles de chaque Karatéka, ainsi que le nombre calculé de "dans", acquis à partir des kata qu'il maîtrise et son age.
CREATE OR REPLACE VIEW public.DETAILS_KARATEKA AS
    SELECT reference, nom, prenom, poids, taille, date_naissance, EXTRACT(years FROM age(date_naissance)) AS age, karateka.ceinture, photo, 
    SUM(CASE WHEN karateka.ceinture = 'noire' THEN nb_dans ELSE 0 END) AS nb_dans
        FROM public.karateka
        INNER JOIN kata.maitrise_kata ON (karateka = reference)
        INNER JOIN kata.kata ON (nom_kata = nom_japonais)
    GROUP BY reference;

-- Lister le nom, prénom, référence, taille, age et le nombre de dans des karatéka par ordre décroissant du nombre de dans
SELECT reference, nom, prenom, taille, age, nb_dans FROM public.DETAILS_KARATEKA ORDER BY nb_dans DESC;


--Récupérer le score des karatékas pour chaque confrontation du kumite.
-- Score du participant1 dans une confrontation de kumite standard
CREATE OR REPLACE VIEW public.SCORE_KUMITE AS
    SELECT karateka.nom, karateka.prenom, karateka.reference as reference_karateka, R1.competition, R1.participant1, R1.participant2, date_confrontation, tour, 
    coalesce(SUM(points), 0) AS score
        FROM kumite.confrontation_kumite AS R1 
        INNER JOIN public.karateka ON (R1.participant1 = karateka.reference)
        LEFT JOIN kumite.coup_kumite AS R2 
            ON (R1.participant1 = R1.participant1 AND R2.participant2 = R2.participant2 AND R1.competition = R2.competition AND R1.participant1 = R2.source)
        LEFT JOIN kumite.autoriser_coup_kumite AS R3 
            ON (R2.competition = R3.competition AND R2.sous_cat = R3.sous_categorie)
    GROUP BY (karateka.reference, R1.participant1, R1.participant2, R1.competition) 
    UNION -- Score du participant1 dans une confrontation de kumite mixte
    SELECT karateka.nom, karateka.prenom, karateka.reference as reference_karateka, R1.competition, R1.participant1, R1.participant2, date_confrontation, tour, 
    coalesce(SUM(points), 0) AS score
        FROM kumite.confrontation_kumite_mixte AS R1 
        INNER JOIN public.karateka ON (R1.participant1 = karateka.reference)
        LEFT JOIN kumite.coup_kumite_mixte AS R2 
            ON (R1.participant1 = R1.participant1 AND R2.participant2 = R2.participant2 AND R1.competition = R2.competition AND R1.participant1 = R2.source)
        LEFT JOIN public.autoriser_coup_kumite_mixte AS R3 
            ON (R2.competition = R3.competition AND R2.sous_cat = R3.sous_categorie)
    GROUP BY (karateka.reference, R1.participant1, R1.participant2, R1.competition) 
    UNION -- Score du participant2 dans une confrontation de kumite standard
    SELECT karateka.nom, karateka.prenom, karateka.reference as reference_karateka, R1.competition, R1.participant1, R1.participant2, date_confrontation, tour, 
    coalesce(SUM(points), 0) AS score
        FROM kumite.confrontation_kumite AS R1 
        INNER JOIN public.karateka ON (R1.participant2 = karateka.reference)
        LEFT JOIN kumite.coup_kumite AS R2 
            ON (R1.participant1 = R1.participant1 AND R2.participant2 = R2.participant2 AND R1.competition = R2.competition AND R1.participant2 = R2.source)
        LEFT JOIN kumite.autoriser_coup_kumite AS R3 
            ON (R2.competition = R3.competition AND R2.sous_cat = R3.sous_categorie)
    GROUP BY (karateka.reference, R1.participant1, R1.participant2, R1.competition) 
    UNION -- Score du participant2 dans une confrontation de kumite mixte
    SELECT karateka.nom, karateka.prenom, karateka.reference as reference_karateka, R1.competition, R1.participant1, R1.participant2, date_confrontation, tour, 
    coalesce(SUM(points), 0) AS score
        FROM kumite.confrontation_kumite_mixte AS R1 
        INNER JOIN public.karateka ON (R1.participant2 = karateka.reference)
        LEFT JOIN kumite.coup_kumite_mixte AS R2 
            ON (R1.participant1 = R1.participant1 AND R2.participant2 = R2.participant2 AND R1.competition = R2.competition AND R1.participant2 = R2.source)
        LEFT JOIN public.autoriser_coup_kumite_mixte AS R3 
            ON (R2.competition = R3.competition AND R2.sous_cat = R3.sous_categorie)
    GROUP BY (karateka.reference, R1.participant1, R1.participant2, R1.competition) ;

-- Pour chaque compétition de kumite, récupérer le nom, référence et prénom de chaque karatéka ainsi que son score total. 
-- Les résultats sont ordonnés par ordre croissant du nom de compétition puis par ordre décroissant du score.
SELECT reference_karateka, nom, prenom, competition, SUM(score) as score_total 
    FROM public.SCORE_KUMITE
    GROUP BY (reference_karateka, nom, prenom, competition) 
    ORDER BY competition ASC, score_total DESC;

--En plus des détails de chaque confrontation de kumite, déterminer le gagnant, le perdant, le score du gagnant et le score du perdant à chaque confrontation.
CREATE OR REPLACE VIEW public.CONFRONTATION_KUMITE_MODIFIEE AS 
    SELECT 
        CASE WHEN r1.score < r2.score then r1.reference_karateka ELSE r2.reference_karateka END AS perdant,
        r1.competition,
        CASE WHEN r1.score > r2.score then r1.reference_karateka ELSE r2.reference_karateka END AS gagnant,
        GREATEST(r1.score, r2.score) as score_gagnant,
        LEAST(r1.score, r2.score) as score_perdant,
        conf.date_confrontation, conf.numero_salle, conf.adresse_salle, conf.tour, false as mixte
        FROM kumite.confrontation_kumite conf
        INNER JOIN public.SCORE_KUMITE r1 ON (r1.reference_karateka = conf.participant1 and r1.participant1 = conf.participant1 and r1.participant2 = conf.participant2 and r1.competition = conf.competition)
        INNER JOIN public.SCORE_KUMITE r2 ON (r2.reference_karateka = conf.participant2 and r2.participant1 = conf.participant1 and r2.participant2 = conf.participant2 and r2.competition = conf.competition)
    UNION 
    SELECT
        CASE WHEN r1.score < r2.score then r1.reference_karateka ELSE r2.reference_karateka END AS perdant,
        r1.competition,
        CASE WHEN r1.score > r2.score then r1.reference_karateka ELSE r2.reference_karateka END AS gagnant,
        GREATEST(r1.score, r2.score) as score_gagnant,
        LEAST(r1.score, r2.score) as score_perdant,
        conf.date_confrontation, conf.numero_salle, conf.adresse_salle, conf.tour, true as mixte
        FROM kumite.confrontation_kumite_mixte conf
        INNER JOIN public.SCORE_KUMITE r1 ON (r1.reference_karateka = conf.participant1 and r1.participant1 = conf.participant1 and r1.participant2 = conf.participant2 and r1.competition = conf.competition)
        INNER JOIN public.SCORE_KUMITE r2 ON (r2.reference_karateka = conf.participant2 and r2.participant1 = conf.participant1 and r2.participant2 = conf.participant2 and r2.competition = conf.competition);


-- Lister le nom, prénom du gagnant et son score, le nom, prénom du perdant et son score à chaque confrontation de kumite.
SELECT 
    competition, tour,
    r1.nom as nom_gagnant, r1.prenom as prenom_gagnant, score_gagnant,
    r2.nom as nom_perdant, r2.prenom as prenom_perdant, score_perdant
FROM public.CONFRONTATION_KUMITE_MODIFIEE 
INNER JOIN public.karateka r1 on (r1.reference = gagnant)
INNER JOIN public.karateka r2 on (r2.reference = perdant)
ORDER BY competition ASC, tour DESC;


--Pour chaque compétition de kata standard, récupérer le nombre de confrontations à chaque tour.
CREATE OR REPLACE VIEW public.NB_TOURS_KATA AS
    SELECT comp.reference, comp.nom, comp.debut, comp.fin, conf.tour, COUNT(perdant) AS nombre_confrontations
        FROM kata.competition_kata comp
        INNER JOIN kata.confrontation_kata conf 
            ON (conf.competition = comp.reference)
        GROUP BY comp.reference, conf.tour
        ORDER BY conf.tour;

--Récupérer le nombre de confrontations en demi finale pour la compétition 'KATA_FR_100'
SELECT nombre_confrontations as nombre_conf_demi_kata_fr_100 FROM NB_TOURS_KATA WHERE tour = 'demi' AND reference = 'KATA_FR_100';

--Vérifier si un autre match en quart de finale pourra avoir lieu dans la compétition 'KATA_FR_100'
SELECT COALESCE((SELECT nombre_confrontations < 4 FROM NB_TOURS_KATA WHERE tour = 'quart' AND reference = 'KATA_FR_100'), true)
    AS conf_quart_kata_peut_avoir_lieu;

--Vérifier si un autre match en demi finale pourra avoir lieu dans la compétition 'KATA_FR_100'
SELECT COALESCE((SELECT nombre_confrontations < 2 FROM NB_TOURS_KATA WHERE tour = 'demi' AND reference = 'KATA_FR_100'), true) 
    AS conf_demi_kata_peut_avoir_lieu ;

--Pour chaque compétition de kata, récupérer toutes les images relatives à cette compétition.
SELECT reference, nom, debut, fin, image FROM kata.competition_kata INNER JOIN kata.posseder_image_kata ON (competition = reference);

--Récupérer tous les professeurs (nom, prénom et age) du club 'Fédération Française de Karaté et Disciplines Associées'.
SELECT reference, nom, prenom, age 
    FROM public.professeur_club 
    INNER JOIN public.DETAILS_KARATEKA ON (karateka = reference)
    WHERE club = 'Fédération Française de Karaté et Disciplines Associées';

--Récupérer toutes les sous-catégorie du mouvement 'Jodan Mae Age Empi Uchi'
SELECT sous_cat FROM mouvements.sous_categorie_mouvement WHERE mouvement = 'Jodan Mae Age Empi Uchi';

--Récupérer toutes les compétitions qui ont commencé en 2018
SELECT * FROM public.TOUTES_LES_COMPETITIONS WHERE debut >= to_date('2018', 'yyyy') AND debut < to_date('2019', 'yyyy');

--Pour chaque compétition de kumite (nom + référence), récupérer le nom des coups autorisés, ainsi que le nombre de points remportés par chaque coup.
SELECT nom, reference, sous_categorie, points 
    FROM kumite.autoriser_coup_kumite, kumite.competition_kumite 
    WHERE reference = competition 
UNION
SELECT reference, nom, sous_categorie, points 
    FROM public.autoriser_coup_kumite_mixte, public.competition_mixte 
    WHERE reference = competition
ORDER BY nom;

--Récupérer La liste ordonnée de mouvements du kata 'Heian Yondan':
SELECT mouvement, position FROM kata.mouvement_kata WHERE nom_kata = 'Heian Yondan' ORDER BY position;

COMMIT;